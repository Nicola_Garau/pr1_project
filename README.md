![s1.PNG](https://bitbucket.org/repo/GBM9zg/images/415125951-s1.PNG) ![s2.PNG](https://bitbucket.org/repo/GBM9zg/images/3608090750-s2.PNG) ![s3.PNG](https://bitbucket.org/repo/GBM9zg/images/1715698767-s3.PNG) ![s4.PNG](https://bitbucket.org/repo/GBM9zg/images/2619139574-s4.PNG) ![Scontrino.PNG](https://bitbucket.org/repo/GBM9zg/images/3771478245-Scontrino.PNG)

# Progetto base (max 20pt/20pt) #
## Il progetto è relativo alla gestione di un ristorante con 15 tavoli e della gestione delle ordinazioni nonché della stampa degli scontrini. ##
### Il programma dovrà permettere le seguenti operazioni: ###

Gestione menu

L’operatore dovrà poter creare e aggiornare l’elenco dei piatti disponibili presso il
ristorante.

Ogni piatto dovrà essere memorizzato all’interno di una struttura contenente:

-­‐ ID, identificativo del piatto;

-­‐ Nome piatto, definito da una stringa minore o uguale ai 100 caratteri;

-­‐ Tipologia (Antipasto, Primo, Secondo, Contorno, Dessert) definito utilizzando
un’enumerazione;

-­‐ Ingredienti, memorizzati in un vettore di stringhe. Ogni piatto dovrà essere
composto (sempre) da tre ingredienti.

-­‐ Prezzo, espresso in euro con due cifre decimali.

L’elenco dei piatti dovrà essere memorizzato all’interno di un file binario, e, attraverso
l’uso di un menu dedicato, dovrà essere quindi possibile:

-­‐ Aggiungere nuovi piatti (evitando doppioni);

-­‐ Modificare qualsiasi campo dei piatti già esistenti;

-­‐ Eliminare piatti precedentemente inseriti (in questo caso prestare particolare
attenzione a come compattare poi l’elenco dei piatti);

-­‐ Stampare l’intero menù;

-­‐ Ricercare e stampare tutti i piatti aventi un certo ingrediente, effettuando una
ricerca sui tre ingredienti che compaiono in ogni piatto.

Gestione cassa

L’operatore dovrà poter gestire i tavoli serviti e le ordinazioni richieste.

Nel momento in cui arriva un’ordinazione, l’utente dovrà creare un nuovo
scontrino/ordinazione sulla base dei piatti ordinati. Nel caso in cui i clienti ordinino altri
piatti in un secondo momento, questi dovranno essere aggiunti allo
scontrino/ordinazione, e, nel momento in cui sarà chiesto il conto, lo scontrino definitivo
dovrà essere stampato.

Lo scontrino sarà memorizzato come una struttura contenente le seguenti informazioni:

-­‐ ID, rappresentante il numero del tavolo;

-­‐ Ordini, la cui organizzazione è lasciata a discrezione dello studente;

-­‐ Prezzo totale, che sarà editato direttamente al momento della stampa.

Per ogni scontrino dovranno inoltre essere gestite le seguenti operazioni:

-­‐ Creazione nuovo scontrino: ogni scontrino sarà relativo ad un tavolo, non
possono esistere più scontrini per uno stesso tavolo.

-­‐ Aggiunta ordini: tale aggiunta dovrà essere basata sull’archivio creato in
precedenza specificando anche la quantità di porzioni richieste;

-­‐ Stampa scontrino: la stampa dovrà seguire l’esempio proposto.

Il calcolo del totale dello scontrino dovrà essere eseguito solo al momento della stampa
e, nel caso in cui l’importo sia maggiore di 40 €, dovrà essere applicato uno sconto sul
totale pari al 10%.

La stampa dello scontrino non avverrà attraverso stampanti ad aghi, a getto d’inchiostro o
laser, ma dovrà essere salvato all’interno di un file di testo.

Ogni nuovo scontrino che va in stampa sovrascriverà quello precedente.

Contestualmente alla stampa dello scontrino dovrà essere gestito un altro file di testo
contenente il totale di tutti gli scontrini stampati, aggiornando il totale e aggiungendo il
nuovo importo.

Lo stampa dello scontrino dovrà quindi avere il seguente schema:

Tavolo: 5

Antipasti

-­‐ 1 x bruschette rustiche 6,00€

Primi piatti

-­‐ 1 x Riso venere con salmone e noci 15,00 €

-­‐ 2 x Spaghetti arselle, carciofi e bottarga 20,00 €

Secondi piatti

-­‐ 1 x braciola di equino con aglio e prezzemolo 10,00 €

Contorni

-­‐ 2 x patate al forno con pancetta e spezie 08,00 €

-­‐

Importo parziale 59,00 €

Valore sconto (10%) 05,90 €

Importo totale 53,10 €

Precisazioni stampa:

-­‐ se una tipologia di piatti non è presente nell’ordinazione non dovrà comparire
nello scontrino (ad esempio i dessert);

-­‐ come si vede nell’esempio dei primi piatti, più porzioni dello stesso piatto
dovranno essere raggruppate;

-­‐ La dicitura “valore sconto” dovrà essere presente solo se applicabile lo sconto.

Precisazioni generiche:

-­‐ Per aumentare la leggibilità del codice è opportuno che ogni azione (aggiunta /
modifica / eliminazione / ricerca piatto / crezione / aggiunta_ordine / stampa
scontrino ecc) siano dichiarate in appositefunzioni.

-­‐ Il progetto deve essere suddiviso in file header e file sorgente.

Consigli:

-­‐ Per chi progettasse la versione senza interfaccia grafica, può usare
occasionalmente la funzione di pulizia dello schermo per pulire la console e
rendere più leggibile il tutto (N.B. La funzione di pulizia dello schermo non è la
stessa su diversi sistemi operativi – vedi compilazione condizionale).

Funzionalità avanzate (max 2pt/20pt)

Le funzionalità avanzate verranno valutate solo se tutte le specifiche di base vengono
rispettate, anche se queste ultime presentano errori.

Tali aggiunte riguardano la gestione delle scorte.

Contestualmente alla creazione dell’archivio dei piatti presenti in menu dovrà essere
realizzato un archivio (file binario o testuale) contenete tutti gli ingredienti dei piatti.

Tale archivio dovrà essere aggiornato dopo la stampa di ogni scontrino tenendo conto di
quante volte un certo ingrediente è stato utilizzato.

Particolare importanza ricade nel fatto che cambiando il menu, l’archivio degli ingredienti
dovrà sempre essere mantenuto e non resettato. Dovranno essere aggiunti eventuali
nuovi ingredienti di eventuali nuovi piatti ma dovranno essere sempre mantenuti tali gli
ingredienti di piatti eliminati dal menu.

Interfaccia grafica (max 3pt/20pt)

L’interfaccia grafica può essere realizzata solo avendo rispettato tutte le specifiche di base
richieste. Potrà realizzare l’interfaccia grafica anche chi non ha implementato funzionalità
avanzate.

L’interfaccia grafica dovrà essere pensata per dispositivi dotati di touch-screen (classici
monitor presenti nei ristoranti per gestire le ordinazioni) utilizzando quindi widget
appropriati all’interazione.

Consegna

La consegna dei progetti avverrà attraverso la piattaforma Moodle caricando
nell’apposito form di upload l’intero progetto sotto forma di archivio zip avente come
nome: FP.cognome.nome.matricola.zip (ad es. FP.nitti.marco.12345.zip).

Il voto massimo è pari a 25/20.

La data di consegna prevista è il 28 febbraio 2014.

Oltre tale data la consegna subirà le seguenti penalizzazioni:

- entro il 31 marzo 2014: - 1pt;

- entro il 30 aprile 2014: - 2pt;

- entro il 31 maggio 2014: - 3pt;

- entro il 30 giugno 2014: - 4pt;

- entro il 31 luglio 2014: - 5pt;

- entro il 30 settembre 2014: - 6pt;

Il progetto può essere consegnato una sola volta.

Coloro che non consegneranno il progetto entro il 30 settembre dovranno ripetere il
corso il prossimo anno accademico.

Il voto minimo per superare il progetto è pari a 10/20. Nel caso in cui il progetto non
superi tale soglia, il corso dovrà essere seguito nuovamente, salvo che non sia possibile
integrare il progetto entro il 30 settembre in modo da ottenere la sufficienza richiesta.

La riconsegna del progetto aggiornerà gli eventuali punti di penalizzazione.

Lo studente che decide di consegnare il progetto con l’utilizzo delle librerie grafiche
(GTK) sosterrà un colloquio orale per la discussione delle scelte implementative e
presentare il funzionamento del progetto.