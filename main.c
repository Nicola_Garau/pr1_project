/** Programma di gestione di un ristorante

    Autore: Nicola Garau
    Matricola: 48985
    Data di consegna: 26/02/2014
    Feedback: alocin.uarag@gmail.com

**/

#include "core.h"   ///Includo il file header relativo al core del programma
#include "gui.h"    ///Includo il file header relativo all'interfaccia del programma
                    /*** N.B. Il programma � stato suddiviso principalmente in due parti: una che si occupa di gestire l'interfaccia grafica del programma (files gui.h e gui.c)
                         ed un'altra che permette di svolgere tutte le operazioni riguardanti il cuore del programma (files core.h e core.c).
                         Tuttavia poich� alcune funzioni delle gtk non permettono di passare pi� di un parametro per volta, in alcune parti di codice riguardanti l'interfaccia
                         � possibile trovare anche delle piccole porzioni riguardanti il core o viceversa. ***/

                    /*** N.B.2 Ho deciso di nascondere la console, se si desidera ripristinarla (dall'IDE Code::Blocks) � necessario andare sulle Propriet� del progetto--->Build Targets--->Type
                         ed impostare "Console Application" al posto di "GUI Application" e fare spunta su "Pause when execution ends" ***/

                    /*** N.B.3 Il programma � prevalentemente case-insensitive e i prezzi decimali vanno inseriti usando la VIRGOLA (non il punto) ***/

int main (int argc, char *argv[])                                  ///Entry point
{
    gtk_init (&argc, &argv);                                       ///Inizializzo le librerie GTK

    gtk_rc_parse("share/themes/MurrinaVerdeOlivo/gtk-2.0/gtkrc");  ///Applico il tema "MurrinaVerdeOlivo" presente in share/themes/. Il file .dll relativi al theme-engine sono nella cartella bin/debug/lib

    Elementigtk *pacchetto;                                        ///Dichiaro un puntatore alla struttura di tipo Elementigtk (vedi core.h per la definizione)

    pacchetto=(Elementigtk*)calloc(1,sizeof(Elementigtk));         ///Alloco dinamicamente il puntatore precedentemente dichiarato

    interfaccia(pacchetto);                                        ///Invoco la funzione che mi permette di caricare la finestra principale del programma

    gtk_main ();                                                   ///Loop di rendering

    free(pacchetto->vettore);                                      ///Libero la memoria allocata per il vettore di piatti (allocata in core.c)

    free(pacchetto);                                               ///Libero la memoria allocata in precedenza per il puntatore alla struttura Elementigtk

    return 0;                                                      ///Exit point
}
