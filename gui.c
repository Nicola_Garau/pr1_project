/**
    In questo file � possibile trovare tutte le definizioni delle funzioni relative all'interfaccia del programma.
    (Tuttavia � possibile trovare alcuni pezzi di codice che si occupano di svolgere delle operazioni riguardanti anche il core).
    Si consiglia di compattare ogni funzione per avere una visione generale di come funziona il programma.
 **/

#include "core.h"       ///Includo il file header core.h nel quale sono contenute tutte le dichiarazioni di funzioni, inclusioni di libreria, ecc. riguardanti il core del programma
#include "gui.h"        ///ed il file gui.h che contiene delle direttive necessarie per far funzionare correttamente l'interfaccia grafica


void interfaccia(Elementigtk *pacchetto)                                                    ///Finestra principale: quella che appare all'avvio del programma (**E' presente una piccola parte di core**)
{
    static Scontrino *scontrini=NULL;                               ///Dichiaro un puntatore alla struttura di tipo Scontrino, viene inizializzata solo una volta
    int i;                                                          ///Dichiaro un contatore
    if(scontrini==NULL)                                             ///Se il file degli scontrini non esiste..
    {
        scontrini=(Scontrino*)calloc(N_TAVOLI, sizeof(Scontrino));  ///Alloco dinamicamente N_TAVOLI*sizeof(Scontrino) bytes che metto a disposizione per il puntatore "scontrini"
        for(i=0; i<N_TAVOLI; i++){                                  ///Faccio scorrere tutti i tavoli
            scontrini[i].id_tavolo=-1;                              ///e imposto l'id di ciascun tavolo a -1..questo mi servir� per distinguere i tavoli che hanno gi� ordinato da quelli che non lo hanno ancora fatto
            //printf("%p scontrini[%d].id_tavolo=%d\n", scontrini, i, scontrini[i].id_tavolo);
            }
    }
    pacchetto->punt_scontrini=scontrini;                            ///Inizializzo un puntatore in modo che punti a "scontrini", per far si che "scontrini" sia visibile da pi� funzioni nel programma (le funzioni GTK accettano il passaggio di un solo valore per volta)
    pacchetto->vettore=NULL;                                        ///Inizializzo a NULL il vettore di piatti
    carica_piatti(pacchetto);                                       ///Trasferisco i piatti presenti nel file binario nel vettore di piatti per evitare di effettuare troppe letture da file
    carica_scorte(pacchetto);                                       ///Carico tutti gli ingredienti presenti nel menu nel file delle scorte: se non sono presenti, verranno aggiunti, se sono presenti non verranno sovrascritti, se il file non esiste verr� creato

    /**Creo la finestra principale e la personalizzo*/
    pacchetto->finestra_principale = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_container_set_border_width (GTK_CONTAINER (pacchetto->finestra_principale), 8);
    gtk_window_set_title (GTK_WINDOW (pacchetto->finestra_principale), "Ristorante da Nico");
    gtk_window_set_default_size(GTK_WINDOW(pacchetto->finestra_principale), 500, 500);
    gtk_window_set_position (GTK_WINDOW (pacchetto->finestra_principale), GTK_WIN_POS_CENTER);
    g_signal_connect (pacchetto->finestra_principale, "destroy", gtk_main_quit, NULL);

    /**Creo una vbox e la inserisco dentro la finestra*/
    GtkWidget *vbox = gtk_vbox_new (TRUE, 6);
    gtk_container_add (GTK_CONTAINER (pacchetto->finestra_principale), vbox);

    /**Creo tre hbox e le inserisco dentro la vbox*/
    GtkWidget *hbox_insegna = gtk_hbox_new (TRUE, 6);
    gtk_box_pack_start (GTK_BOX (vbox), hbox_insegna, TRUE, TRUE, 0);

    GtkWidget *hbox1 = gtk_hbox_new (TRUE, 6);
    gtk_box_pack_start (GTK_BOX (vbox), hbox1, TRUE, TRUE, 0);

    GtkWidget *hbox2 = gtk_hbox_new (TRUE, 6);
    gtk_box_pack_start (GTK_BOX (vbox), hbox2, TRUE, TRUE, 0);

    /**Carico un'immagine e la inserisco dentro la prima hbox*/
    GtkWidget *insegna=gtk_image_new_from_file("Insegna.png");
    gtk_box_pack_start (GTK_BOX (hbox_insegna), insegna, TRUE, TRUE, 0);

    /**Creo 4 pulsanti, ne inserisco 2 nella seconda hbox e gli altri 2 nella terza.
       Alla pressione di ciascuno dei 4 pulsanti collego una procedura diversa*/
    pacchetto->pulsante_menu=gtk_button_new_with_label("Gestione Menu");
    gtk_box_pack_start (GTK_BOX (hbox1), pacchetto->pulsante_menu, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_menu), "clicked", G_CALLBACK(gestione_menu), (gpointer)pacchetto);    ///Invoco la procedura di gestione del menu
    pacchetto->pulsante_cassa=gtk_button_new_with_label("Gestione Cassa");
    gtk_box_pack_start (GTK_BOX (hbox1), pacchetto->pulsante_cassa, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_cassa), "clicked", G_CALLBACK(gestione_cassa), (gpointer)scontrini);  ///Invoco la procedura di gestione della cassa
    pacchetto->pulsante_scorte=gtk_button_new_with_label("Gestione Scorte");
    gtk_box_pack_start (GTK_BOX (hbox2), pacchetto->pulsante_scorte, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_scorte), "clicked", G_CALLBACK(stampa_scorte), (gpointer)scontrini);  ///Invoco la procedura di gestione delle scorte
    pacchetto->pulsante_esci=gtk_button_new_with_label("Esci");
    gtk_box_pack_start (GTK_BOX (hbox2), pacchetto->pulsante_esci, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_esci), "clicked", G_CALLBACK(dealloca), (gpointer)scontrini);         ///Invoco la procedura di chiusura del programma

    gtk_widget_show_all (pacchetto->finestra_principale);                                                               ///Rendo visibile il tutto
}

void gestione_menu(GtkWidget *pulsante_menu, Elementigtk *pacchetto)                        ///Da qui � possibile gestire i piatti (la lora aggiunta, modifica..)
{
    printf("Ok menu\n");
    /**Creo una finestra e la personalizzo*/
    pacchetto->finestra_menu=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_container_set_border_width (GTK_CONTAINER (pacchetto->finestra_menu), 8);
    gtk_window_set_title (GTK_WINDOW (pacchetto->finestra_menu), "Menu");
    gtk_window_set_default_size(GTK_WINDOW(pacchetto->finestra_menu), 500, 780);
    gtk_window_set_position (GTK_WINDOW (pacchetto->finestra_menu), GTK_WIN_POS_CENTER);

    /**Inserisco una vbox dentro la finestra principale*/
    GtkWidget *vbox=gtk_vbox_new(TRUE, 6);
    gtk_container_add (GTK_CONTAINER (pacchetto->finestra_menu), vbox);

    /**Creo 6 pulsanti, li inserisco dentro la vbox e li coloro*/
    pacchetto->pulsante_aggiungi=gtk_button_new_with_label("Aggiungi un piatto");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->pulsante_aggiungi, TRUE, TRUE, 0);
    GdkColor color;
    gdk_color_parse ("#99FF66", &color);
    gtk_widget_modify_bg ( GTK_WIDGET(pacchetto->pulsante_aggiungi), GTK_STATE_NORMAL, &color);
    pacchetto->pulsante_modifica=gtk_button_new_with_label("Modifica un piatto");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->pulsante_modifica, TRUE, TRUE, 0);
    gdk_color_parse ("#CCFFFF", &color);
    gtk_widget_modify_bg ( GTK_WIDGET(pacchetto->pulsante_modifica), GTK_STATE_NORMAL, &color);
    pacchetto->pulsante_elimina=gtk_button_new_with_label("Elimina un piatto");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->pulsante_elimina, TRUE, TRUE, 0);
    gdk_color_parse ("#FF734B", &color);
    gtk_widget_modify_bg ( GTK_WIDGET(pacchetto->pulsante_elimina), GTK_STATE_NORMAL, &color);
    pacchetto->pulsante_stampa=gtk_button_new_with_label("Visualizza il menu");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->pulsante_stampa, TRUE, TRUE, 0);
    gdk_color_parse ("#CC99FF", &color);
    gtk_widget_modify_bg ( GTK_WIDGET(pacchetto->pulsante_stampa), GTK_STATE_NORMAL, &color);
    pacchetto->pulsante_ricerca=gtk_button_new_with_label("Ricerca per ingrediente");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->pulsante_ricerca, TRUE, TRUE, 0);
    gdk_color_parse ("#FBB117", &color);
    gtk_widget_modify_bg ( GTK_WIDGET(pacchetto->pulsante_ricerca), GTK_STATE_NORMAL, &color);
    pacchetto->pulsante_indietro=gtk_button_new_with_label("Indietro");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->pulsante_indietro, TRUE, TRUE, 0);
    gdk_color_parse ("#B8B8B8", &color);
    gtk_widget_modify_bg ( GTK_WIDGET(pacchetto->pulsante_indietro), GTK_STATE_NORMAL, &color);

    /**Alla pressione di ciascuno dei 6 pulsanti collego una procedura diversa*/
    g_signal_connect(G_OBJECT(pacchetto->pulsante_aggiungi), "clicked", G_CALLBACK(aggiungi_piatto), (gpointer)pacchetto);          ///Invoco la procedura di aggiunta di un piatto al menu
    g_signal_connect(G_OBJECT(pacchetto->pulsante_modifica), "clicked", G_CALLBACK(modifica_piatto), (gpointer)pacchetto);          ///Invoco la procedura di modifica di un piatto del menu
    g_signal_connect(G_OBJECT(pacchetto->pulsante_stampa), "clicked", G_CALLBACK(stampa), (gpointer)pacchetto);                     ///Invoco la procedura di stampa del menu
    g_signal_connect(G_OBJECT(pacchetto->pulsante_elimina), "clicked", G_CALLBACK(elimina_piatto), (gpointer)pacchetto);            ///Invoco la procedura di eliminazione di un piatto dal menu
    g_signal_connect(G_OBJECT(pacchetto->pulsante_ricerca), "clicked", G_CALLBACK(ricerca_piatti), (gpointer)pacchetto);            ///Invoco la procedura di ricerca di piatti nel menu
    g_signal_connect(G_OBJECT(pacchetto->pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto->finestra_menu); ///Invoco la procedura che mi permette di tornare alla finestra precedente

    gtk_widget_show_all(pacchetto->finestra_menu);                                                                                  ///Rendo il tutto visibile
}

void aggiungi_piatto(GtkWidget(*pulsante_aggiungi), Elementigtk *pacchetto)                 ///Finestra adibita all'aggiunta di nuovi piatti
{
    FILE *elenco_piatti=NULL;   ///Dichiaro un puntatore a file
    Piatto leggi;               ///e una variabile di tipo Piatto

    /**Creo una finestra e la personalizzo*/
    pacchetto->finestra_aggiungi_piatto=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (pacchetto->finestra_aggiungi_piatto), "Aggiungi un piatto");
    gtk_window_set_default_size(GTK_WINDOW(pacchetto->finestra_aggiungi_piatto), 600, 780);
    gtk_window_set_position (GTK_WINDOW (pacchetto->finestra_aggiungi_piatto), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (pacchetto->finestra_aggiungi_piatto) ,15);

    /**Inserisco una vbox dentro la finestra*/
    GtkWidget *vbox=gtk_vbox_new (FALSE, 6);
    gtk_container_add (GTK_CONTAINER (pacchetto->finestra_aggiungi_piatto), vbox);

    /**Inserisco un'icona, una vbox e una hbox dentro la vbox*/
    GtkWidget *icona=gtk_image_new_from_file("aggiungi.png");
    gtk_box_pack_start (GTK_BOX (vbox), icona, TRUE, TRUE, 0);
    GtkWidget *top=gtk_vbox_new (TRUE, 6);
    gtk_box_pack_start (GTK_BOX (vbox), top, TRUE, TRUE, 0);
    GtkWidget *bottom=gtk_hbox_new (TRUE, 6);
    gtk_box_pack_start (GTK_BOX (vbox), bottom, FALSE, FALSE, 0);

    /**Creo due pulsanti e li inserisco dentro la hbox*/
    pacchetto->pulsante_ok=gtk_button_new_from_stock(GTK_STOCK_OK);
    gtk_button_set_label((GtkButton*)pacchetto->pulsante_ok, "\nOk\n");
    gtk_box_pack_start (GTK_BOX (bottom), pacchetto->pulsante_ok, TRUE, TRUE, 0);
    pacchetto->pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (bottom), pacchetto->pulsante_indietro, TRUE, TRUE, 0);

    /**Alla pressione del pulsante "Indietro" invoco la funzione distruggi*/
    g_signal_connect(G_OBJECT(pacchetto->pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto->finestra_aggiungi_piatto);

    /**Creo una entry per il nome del piatto da inserire e la inserisco dentro la vbox, specificando anche la dimensione massima*/
    pacchetto->nome = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->nome), "Nome del piatto");
    gtk_entry_set_max_length(GTK_ENTRY(pacchetto->nome) , NOME);
    gtk_box_pack_start(GTK_BOX(top), pacchetto->nome, TRUE, TRUE, 0);

    /**Creo una hbox che conterr� i pulsanti di scelta della tipologia del piatto e la inserisco dentro la vbox*/
    GtkWidget *box_tipologie=gtk_hbox_new(FALSE, 6);
    gtk_box_pack_start(GTK_BOX(top), box_tipologie, TRUE, TRUE, 0);

    /**con questo controllo mi assicuro che se l'utente non specifica la tipologia, il piatto venga inserito come antipasto*/
    pacchetto->tip_enum=antipasto;

    /**Invoco la funzione che mi permette di creare i pulsanti radio*/
    pulsanti_radio(pacchetto, box_tipologie);

    /**Creo 4 entry: tre per gli ingredienti e una per il prezzo, e le inserisco dentro la vbox*/
    pacchetto->ingrediente1 = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->ingrediente1), "Ingrediente #1");
    gtk_box_pack_start(GTK_BOX(top), pacchetto->ingrediente1, TRUE, TRUE, 0);

    pacchetto->ingrediente2 = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->ingrediente2), "Ingrediente #2");
    gtk_box_pack_start(GTK_BOX(top), pacchetto->ingrediente2, TRUE, TRUE, 0);

    pacchetto->ingrediente3 = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->ingrediente3), "Ingrediente #3");
    gtk_box_pack_start(GTK_BOX(top), pacchetto->ingrediente3, TRUE, TRUE, 0);

    pacchetto->prezzo = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->prezzo), "Prezzo del piatto");
    gtk_box_pack_start(GTK_BOX(top), pacchetto->prezzo, TRUE, TRUE, 0);

    /**Apro il file dei piatti in modalit� ibrida append per verificare se ci sono dei piatti caricati oppure no.
       Ho deciso di controllare nel file e non nel vettore perch� devo essere certo che il file esista in questo preciso punto del programma e che non sia corrotto*/
    elenco_piatti=fopen("piatti.dat", "a+b");
    if(elenco_piatti==NULL)                                         ///Se il file non esiste esco;
    {
        printf("Apertura del file piatti.dat non riuscita\n");
        exit(-1);
    }
    fseek(elenco_piatti, 0, SEEK_SET);                              ///Mi sposto all'inizio del file

    if(fread(&leggi, sizeof(Piatto), 1, elenco_piatti)==0){         ///Se non riesco a leggere niente dal file
        printf("\nIl file e' vuoto\n");
        fclose(elenco_piatti);                                      ///Chiudo il file e invoco la funzione che mi permette di aggiungere il primo piatto

        g_signal_connect(G_OBJECT(pacchetto->pulsante_ok), "clicked", G_CALLBACK(inserisci_primo_piatto), (gpointer)pacchetto);
        g_signal_connect(G_OBJECT(pacchetto->pulsante_ok), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto->finestra_aggiungi_piatto);

        gtk_widget_show_all(pacchetto->finestra_aggiungi_piatto);   ///Rendo il tutto visibile
    }
    else{                                                           ///Altrimenti...
        printf("\nIl file non e' vuoto\n");
        fclose(elenco_piatti);                                      ///Chiudo il file e invoco la funzione che mi permette di aggiungere i piatti successivi al primo

        g_signal_connect(G_OBJECT(pacchetto->pulsante_ok), "clicked", G_CALLBACK(inserisci_piatto), (gpointer)pacchetto);
        g_signal_connect(G_OBJECT(pacchetto->pulsante_ok), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto->finestra_aggiungi_piatto);

        gtk_widget_show_all(pacchetto->finestra_aggiungi_piatto);   ///Rendo il tutto visibile
    }
}

void modifica_piatto(GtkWidget(*pulsante_modifica), Elementigtk *pacchetto)                 ///Questa finestra serve per modificare i "campi" di ogni piatto presente nel menu
{
    printf("\nOk modifica\n");
    /**Creo una nuova finestra e la personalizzo*/
    pacchetto->finestra_modifica_piatto=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (pacchetto->finestra_modifica_piatto), "Modifica un piatto");
    gtk_window_set_default_size(GTK_WINDOW(pacchetto->finestra_modifica_piatto), 800, 900);
    gtk_window_set_position (GTK_WINDOW (pacchetto->finestra_modifica_piatto), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (pacchetto->finestra_modifica_piatto) ,15);

    /**Inserisco una vbox dentro la finestra*/
    GtkWidget *vbox=gtk_vbox_new (FALSE, 6);
    gtk_container_add (GTK_CONTAINER (pacchetto->finestra_modifica_piatto), vbox);

    /**Inserisco un'icona e una label dentro la vbox*/
    GtkWidget *icona=gtk_image_new_from_file("modifica.png");
    gtk_box_pack_start (GTK_BOX (vbox), icona, TRUE, TRUE, 0);
    GtkWidget *label=gtk_label_new("Inserisci il nome del piatto da modificare e scegli il campo che vuoi modificare\nPer confermare fai click sulle icone in basso.");
    gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);

    /**Inserisco due entry dentro la bvox: una per il nome del piatto e una per il nuovo nome*/
    pacchetto->entry = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->entry), "Nome del piatto da modificare");
    gtk_entry_set_max_length(GTK_ENTRY(pacchetto->entry) , NOME);
    gtk_box_pack_start(GTK_BOX(vbox), pacchetto->entry, TRUE, TRUE, 0);

    pacchetto->e_nome = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->e_nome), "Nuovo nome del piatto");
    gtk_entry_set_max_length(GTK_ENTRY(pacchetto->e_nome) , NOME);
    gtk_box_pack_start(GTK_BOX(vbox), pacchetto->e_nome, TRUE, TRUE, 0);

    /**Inserisco una label dentro la vbox ed una hbox per contenere i pulsanti radio*/
    GtkWidget *label2=gtk_label_new("Seleziona una di queste caselle se vuoi modificare la tipologia.");
    gtk_box_pack_start (GTK_BOX (vbox), label2, TRUE, TRUE, 0);

    GtkWidget *hbox=gtk_hbox_new (TRUE, 6);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

    /**Invoco la funzione che mi permette di creare i pulsanti radio da inserire dentro la hbox */
    pulsanti_radio(pacchetto, hbox);

    /**Inserisco dentro la vbox 4 entry: tre per gli ingredienti da modificare e una per il prezzo da modificare*/
    pacchetto->e_ing1 = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->e_ing1), "Nuovo ingrediente #1");
    gtk_box_pack_start(GTK_BOX(vbox), pacchetto->e_ing1, TRUE, TRUE, 0);

    pacchetto->e_ing2 = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->e_ing2), "Nuovo ingrediente #2");
    gtk_box_pack_start(GTK_BOX(vbox), pacchetto->e_ing2, TRUE, TRUE, 0);

    pacchetto->e_ing3 = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->e_ing3), "Nuovo ingrediente #3");
    gtk_box_pack_start(GTK_BOX(vbox), pacchetto->e_ing3, TRUE, TRUE, 0);

    pacchetto->e_prezzo = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->e_prezzo), "Nuovo prezzo");
    gtk_box_pack_start(GTK_BOX(vbox), pacchetto->e_prezzo, TRUE, TRUE, 0);

    /**Inserisco una label ed una hbox dentro la vbox*/
    GtkWidget *testo=gtk_label_new("Cosa vuoi modificare?");
    gtk_box_pack_start(GTK_BOX(vbox), testo, TRUE, TRUE, 0);

    GtkWidget *hbox2=gtk_hbox_new (FALSE, 6);
    gtk_box_pack_start (GTK_BOX (vbox), hbox2, TRUE, TRUE, 0);

    /**Inserisco 6 pulsanti dentro la hbox e collego alla loro pressione diverse funzioni*/
    pacchetto->nome=gtk_button_new_with_label("Nome");
    gtk_box_pack_start (GTK_BOX (hbox2), pacchetto->nome, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->nome), "clicked", G_CALLBACK(modifica_nome), (gpointer)pacchetto);                 ///Invoco la procedura che mi permette di cambiare il nome
    pacchetto->p_tip=gtk_button_new_with_label("Tipologia");
    gtk_box_pack_start (GTK_BOX (hbox2), pacchetto->p_tip, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->p_tip), "clicked", G_CALLBACK(modifica_tipologia), (gpointer)pacchetto);           ///Invoco la procedura che mi permette di cambiare la tipologia
    pacchetto->ingrediente1=gtk_button_new_with_label("Ingrediente 1");
    gtk_box_pack_start (GTK_BOX (hbox2), pacchetto->ingrediente1, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->ingrediente1), "clicked", G_CALLBACK(modifica_ingrediente), (gpointer)pacchetto);  ///Invoco la procedura che mi permette di cambiare il primo ingrediente
    pacchetto->ingrediente2=gtk_button_new_with_label("Ingrediente 2");
    gtk_box_pack_start (GTK_BOX (hbox2), pacchetto->ingrediente2, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->ingrediente2), "clicked", G_CALLBACK(modifica_ingrediente), (gpointer)pacchetto);  ///Invoco la procedura che mi permette di cambiare il secondo ingrediente
    pacchetto->ingrediente3=gtk_button_new_with_label("Ingrediente 3");
    gtk_box_pack_start (GTK_BOX (hbox2), pacchetto->ingrediente3, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->ingrediente3), "clicked", G_CALLBACK(modifica_ingrediente), (gpointer)pacchetto);  ///Invoco la procedura che mi permette di cambiare il terzo ingrediente
    pacchetto->prezzo=gtk_button_new_with_label("Prezzo");
    gtk_box_pack_start (GTK_BOX (hbox2), pacchetto->prezzo, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->prezzo), "clicked", G_CALLBACK(modifica_prezzo), (gpointer)pacchetto);             ///Invoco la procedura che mi permette di cambiare il prezzo

    /**Creo un pulsante che mi permette di ritornare alla finestra precedente*/
    pacchetto->pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto->finestra_modifica_piatto);

    gtk_widget_show_all(pacchetto->finestra_modifica_piatto);                                                               ///Rendo il tutto visibile
}

void elimina_piatto(GtkWidget(*pulsante_elimina), Elementigtk *pacchetto)                   ///Finestra per mezzo della quale � possibile eliminare uno o tutti i piatti del menu
{
    printf("Ok elimina\n");
    /**Creo una finestra e la personalizzo*/
    pacchetto->finestra_elimina_piatto=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (pacchetto->finestra_elimina_piatto), "Elimina un piatto");
    gtk_window_set_default_size(GTK_WINDOW(pacchetto->finestra_elimina_piatto), 500, 780);
    gtk_window_set_position (GTK_WINDOW (pacchetto->finestra_elimina_piatto), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (pacchetto->finestra_elimina_piatto) ,15);

    /**Inserisco una vbox dentro la finestra principale*/
    GtkWidget *vbox=gtk_vbox_new (FALSE, 6);
    gtk_container_add (GTK_CONTAINER (pacchetto->finestra_elimina_piatto), vbox);

    /**Inserisco un'immagine e una label dentro la vbox*/
    GtkWidget *icona=gtk_image_new_from_file("elimina.png");
    gtk_box_pack_start (GTK_BOX (vbox), icona, TRUE, TRUE, 0);
    GtkWidget *label=gtk_label_new("Inserisci il nome del piatto da eliminare e premi ok per confermare.");
    gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);

    /**Inserisco una entry ed un pulsante dentro la vbox*/
    pacchetto->entry = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->entry), "Nome del piatto da eliminare");
    gtk_entry_set_max_length(GTK_ENTRY(pacchetto->entry) , NOME);
    gtk_box_pack_start(GTK_BOX(vbox), pacchetto->entry, TRUE, TRUE, 0);
    GtkWidget *pulsante=gtk_button_new_with_label("Ok");
    gtk_box_pack_start(GTK_BOX(vbox), pulsante, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pulsante), "clicked", G_CALLBACK(elimina_un_piatto), (gpointer)pacchetto);    ///Collego al pulsante la funzione di eliminazione di un piatto

    /**Inserisco una entry ed un pulsante dentro la vbox*/
    GtkWidget *label2=gtk_label_new("\nAltrimenti...\n");
    gtk_box_pack_start (GTK_BOX (vbox), label2, FALSE, FALSE, 0);
    GtkWidget *pulsante2=gtk_button_new_with_label("Elimina l'intero file menu");
    gtk_box_pack_start(GTK_BOX(vbox), pulsante2, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pulsante2), "clicked", G_CALLBACK(elimina_tutto), (gpointer)pacchetto);       ///Collego al pulsante la funzione di eliminazione di tutti i piatti

    /**Creo un pulsante che mi permette di tornare alla finestra precedente*/
    pacchetto->pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto->finestra_elimina_piatto);

    gtk_widget_show_all(pacchetto->finestra_elimina_piatto);                                                ///Rendo il tutto visibile
}

void stampa (GtkWidget(*pulsante_stampa), Elementigtk *pacchetto)                           ///Finestra di visualizzazione del menu (**Contiene delle parti di core**)
{
    Piatto buffer;                  ///Dichiaro una variabile di tippo Piatto
    char stringa[NOME]="\0";        ///Dichiaro una stringa che inserir� in delle label dopo aver inizializzato
    int i;                          ///Dichiaro un contatore
    if(pacchetto->dim_vettore==0)   ///Se il menu non contiene alcun piatto avviso l'utente ed esco
    {
        avvisa(2);
        return;
    }
    /**Creo una finestra e la personalizzo*/
    pacchetto->finestra_stampa=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (pacchetto->finestra_stampa), "Il nostro menu");
    gtk_window_set_default_size(GTK_WINDOW(pacchetto->finestra_stampa), 1400, 800);
    gtk_window_set_position (GTK_WINDOW (pacchetto->finestra_stampa), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (pacchetto->finestra_stampa) ,15);

    /**Inserisco una vbox dentro la finestra, una hbox dentro la vbox e un'immagine dentro la hbox*/
    GtkWidget *vbox_main=gtk_vbox_new(FALSE, 6);
    gtk_container_add (GTK_CONTAINER (pacchetto->finestra_stampa), vbox_main);
    GtkWidget *hbox_icona=gtk_hbox_new(FALSE, 6);
    gtk_box_pack_start (GTK_BOX (vbox_main), hbox_icona, FALSE, FALSE, 0);
    GtkWidget *icona=gtk_image_new_from_file("Menu.png");
    gtk_box_pack_start (GTK_BOX (hbox_icona), icona, TRUE, TRUE, 0);
    /**Inserisco una scrolled_window dentro la vbox ed una vbox dentro la finestra scrolled*/
    pacchetto->finestra_scrolled=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(vbox_main), pacchetto->finestra_scrolled);
    GtkWidget *vbox=gtk_vbox_new(FALSE, 6);
    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (pacchetto->finestra_scrolled), vbox);

    /**Creo 5 frames (uno per ogni tipologia), li personalizzo, li inserisco dentro l'ultima vbox creata e inserisco una hbox dentro ogni frame*/
    GtkWidget *frame_antipasti=gtk_frame_new("\n----------ANTIPASTI----------");
    gtk_frame_set_label_align((GtkFrame*)frame_antipasti, 0.5, 0.8);
    gtk_box_pack_start (GTK_BOX (vbox), frame_antipasti, TRUE, TRUE, 0);
    GtkWidget *hbox_antipasti=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_antipasti), hbox_antipasti);

    GtkWidget *frame_primi=gtk_frame_new("\n----------PRIMI----------");
    gtk_box_pack_start (GTK_BOX (vbox), frame_primi, TRUE, TRUE, 0);
    gtk_frame_set_label_align((GtkFrame*)frame_primi, 0.5, 0.8);
    GtkWidget *hbox_primi=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_primi), hbox_primi);

    GtkWidget *frame_secondi=gtk_frame_new("\n----------SECONDI----------");
    gtk_box_pack_start (GTK_BOX (vbox), frame_secondi, TRUE, TRUE, 0);
    gtk_frame_set_label_align((GtkFrame*)frame_secondi, 0.5, 0.8);
    GtkWidget *hbox_secondi=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_secondi), hbox_secondi);

    GtkWidget *frame_contorni=gtk_frame_new("\n----------CONTORNI----------");
    gtk_box_pack_start (GTK_BOX (vbox), frame_contorni, TRUE, TRUE, 0);
    gtk_frame_set_label_align((GtkFrame*)frame_contorni, 0.5, 0.8);
    GtkWidget *hbox_contorni=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_contorni), hbox_contorni);

    GtkWidget *frame_dessert=gtk_frame_new("\n----------DESSERT----------");
    gtk_box_pack_start (GTK_BOX (vbox), frame_dessert, TRUE, TRUE, 0);
    gtk_frame_set_label_align((GtkFrame*)frame_dessert, 0.5, 0.8);
    GtkWidget *hbox_dessert=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_dessert), hbox_dessert);

    /**Creo un pulsante che mi permette di tornare alla finestra precedente*/
    pacchetto->pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox_main), pacchetto->pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto->finestra_stampa);


    for(i=0; i<pacchetto->dim_vettore; i++)     ///Faccio scorrere tutti i piatti
    {
        buffer=pacchetto->vettore[i];           ///Memorizzo per comodit� (e per evitare la contaminazione dei dati del vettore) il piatto corrente in una variabile buffer
        GtkWidget *label=NULL;                  ///Dichiaro e inizializzo una label che inserir� in un frame diverso a seconda della tipologia

        /**A seconda della tipologia del piatto corrente, invoco una funzione che mi permette di stampare una label
           contenente le informazioni relative al piatto all'interno del frame adeguato*/
        switch(buffer.tipologia_piatto)
        {
            case 0:
                stampa_pietanza(buffer, label, stringa, hbox_antipasti);
                break;
            case 1:
                stampa_pietanza(buffer, label, stringa, hbox_primi);
                break;
            case 2:
                stampa_pietanza(buffer, label, stringa, hbox_secondi);
                break;
            case 3:
                stampa_pietanza(buffer, label, stringa, hbox_contorni);
                break;
            case 4:
                stampa_pietanza(buffer, label, stringa, hbox_dessert);
                break;
            default:
                printf("Errore nello switch-case\n");
                break;
        }
    }
    gtk_widget_show_all(pacchetto->finestra_stampa);    ///Rendo il tutto visibile
}

void ricerca_piatti(GtkWidget(*pulsante_ricerca), Elementigtk *pacchetto)                   ///Finestra di ricerca per ingrediente
{
    printf("Ok ricerca\n");
    /**Creo una finestra, la personalizzo e inserisco all'interno una vbox*/
    pacchetto->finestra_ricerca=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (pacchetto->finestra_ricerca), "Ricerca");
    gtk_window_set_default_size(GTK_WINDOW(pacchetto->finestra_ricerca), 600, 450);
    gtk_window_set_position (GTK_WINDOW (pacchetto->finestra_ricerca), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (pacchetto->finestra_ricerca) ,15);
    GtkWidget *vbox=gtk_vbox_new(FALSE, 6);
    gtk_container_add (GTK_CONTAINER (pacchetto->finestra_ricerca), vbox);

    /**Creo un'icona, una label, una entry, due pulsanti e inserisco il tutto dentro la vbox*/
    GtkWidget *icona=gtk_image_new_from_file("cerca.png");
    gtk_box_pack_start (GTK_BOX (vbox), icona, TRUE, TRUE, 0);
    GtkWidget *label=gtk_label_new("Inserisci un ingrediente per visualizzare tutti i piatti associati ad esso");
    gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
    pacchetto->entry=gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(pacchetto->entry), "Ingrediente da cercare");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->entry, TRUE, TRUE, 0);
    GtkWidget *button=gtk_button_new_with_label("Ok");
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(ricerca_per_ingrediente), (gpointer)pacchetto);    ///Invoco la procedura chemi permette di effettuare una ricerca per ingrediente
    pacchetto->pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox), pacchetto->pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto->finestra_ricerca);

    gtk_widget_show_all(pacchetto->finestra_ricerca);       ///Rendo il tutto visibile
}

void gestione_cassa(GtkWidget *pulsante_cassa, Scontrino scontrini[N_TAVOLI])               ///Da qui � possibile gestire le ordinazioni per ciascun tavolo
{
    int i;                          ///Dichiaro un contatore
    char stringa[20];               ///Dichiaro una stringa,
    GtkWidget *box;                 ///un box,
    GtkWidget *p_tavolo[N_TAVOLI];  ///un vettore di pulsanti
    GtkWidget *icon;                ///e un'icona

    printf("Ok cassa\n");
    /**Creo una finestra, la personalizzo e inserisco all'interno una vbox*/
    GtkWidget *finestra_tavoli=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (finestra_tavoli), "Selezione tavolo");
    gtk_window_set_default_size(GTK_WINDOW(finestra_tavoli), 500, 780);
    gtk_window_set_position (GTK_WINDOW (finestra_tavoli), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (finestra_tavoli) ,15);
    GtkWidget *vbox_main=gtk_vbox_new(FALSE, 6);
    gtk_container_add (GTK_CONTAINER (finestra_tavoli), vbox_main);

    /**Creo una hbox e all'interno ci inserisco un'icona e una label*/
    GtkWidget *hbox=gtk_hbox_new(FALSE, 6);
    gtk_box_pack_start (GTK_BOX (vbox_main), hbox, FALSE, FALSE, 0);
    GtkWidget *icona=gtk_image_new_from_file("tavolo.png");
    gtk_box_pack_start (GTK_BOX (hbox), icona, FALSE, FALSE, 0);
    GtkWidget *label=gtk_label_new("Seleziona un tavolo per gestire\nle ordinazioni e gli scontrini");
    gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

    /**Inserisco una finestra scrolled dentro la vbox e ci inserisco all'interno un'altra vbox*/
    GtkWidget* finestra_scrolled=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(vbox_main), finestra_scrolled);
    GtkWidget *vbox=gtk_vbox_new(FALSE, 6);
    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (finestra_scrolled), vbox);

    for(i=0; i<N_TAVOLI; i++)                                                                                   ///Faccio scorrere tutti i tavoli
    {
        sprintf(stringa, "Tavolo %d", i+1);                                                                     ///Stampo su una stringa l'id del tavolo
        box=gtk_hbox_new(FALSE, 6);                                                                             ///Creo una hbox e la inserisco dentro la vbox
        gtk_container_add (GTK_CONTAINER (vbox), box);
        if(scontrini[i].id_tavolo==-1)                                                                          ///Se il tavolo corrente non ha ancora ordinato..
            icon=gtk_image_new_from_file("cameriere.png");                                                      ///inserisco una determinata immagine dentro l'hbox
        else                                                                                                    ///Altrimenti..
            icon=gtk_image_new_from_file("cameriere2.png");                                                     ///ne inserisco una diversa
        gtk_box_pack_start (GTK_BOX (box), icon, FALSE, FALSE, 0);
        p_tavolo[i]=gtk_button_new_with_label(stringa);                                                         ///Creo un pulsante e lo inserisco dentro l'hbox
        gtk_box_pack_start (GTK_BOX (box), p_tavolo[i], TRUE, TRUE, 0);
        g_signal_connect(G_OBJECT(p_tavolo[i]), "clicked", G_CALLBACK(scegli_operazione), (gpointer)scontrini); ///Invoco la procedura che mi permette di scegliere l'operazione da effettuare una scelta per il tavolo corrente
    }
    /**Creo un pulsante che mi permette di tornare alla finestra precedente*/
    GtkWidget *pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox_main), pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)finestra_tavoli);

    g_signal_connect(G_OBJECT(finestra_tavoli), "focus-out-event", G_CALLBACK(refresh), (gpointer)scontrini);   ///Se la finestra viene messa in secondo piano la chiudo, cos� quando verr� riaperta il suo contenuto sar� aggiornato!

    gtk_widget_show_all(finestra_tavoli);                                                                       ///Rendo il tutto visibile
}

void scegli_operazione(GtkWidget *pulsante_operazione, Scontrino scontrini[N_TAVOLI])       ///Finestra di scelta (ordinazione/stampa dello scontrino)
{
    printf("\nOk scegli operazione\n");

    char stringa[20], temp[20], str_p1[30], str_p2[30], str_p3[40];                         ///Dichiaro 5 stringhe che user� in diversi modi
    int numero_tavolo;                                                                      ///e un intero in cui memorizzer� l'id del tavolo
    strcpy(stringa, gtk_button_get_label((GtkButton*)pulsante_operazione));                 ///Copio in stringa il contenuto del pulsante
    sscanf(stringa, "%s %d", temp, &numero_tavolo);                                         ///Leggo dalla stringa l'id del tavolo
    sprintf(str_p1, "Tavolo %d: prendi ordinazione", numero_tavolo);                        ///Stampo tre stringhe che diventeranno label di 3 pulsanti
    sprintf(str_p2, "Tavolo %d: stampa scontrino", numero_tavolo);
    sprintf(str_p3, "Tavolo %d: visualizza ordinazioni", numero_tavolo);
    printf("La stringa vale %s e il numero del tavolo e' %d\n", stringa, numero_tavolo);

    /**Creo una finestra in cui inserisco una vbox*/
    GtkWidget *finestra_ordinazioni=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (finestra_ordinazioni), "Gestione Tavolo");
    gtk_window_set_default_size(GTK_WINDOW(finestra_ordinazioni), 600,900);
    gtk_window_set_position (GTK_WINDOW (finestra_ordinazioni), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (finestra_ordinazioni) ,15);
    GtkWidget *vbox=gtk_vbox_new(TRUE, 6);
    gtk_container_add (GTK_CONTAINER (finestra_ordinazioni), vbox);
    /**Creo una hbox in cui inserisco 2 icone*/
    GtkWidget *hbox_top=gtk_hbox_new(TRUE, 6);
    gtk_container_add (GTK_CONTAINER (vbox), hbox_top);
    GtkWidget *icona1=gtk_image_new_from_file("ordinazioni.png");
    gtk_box_pack_start (GTK_BOX (hbox_top), icona1, TRUE, TRUE, 0);
    GtkWidget *icona2=gtk_image_new_from_file("scontrino.png");
    gtk_box_pack_start (GTK_BOX (hbox_top), icona2, TRUE, TRUE, 0);
    /**Creo una hbox in cui inserisco 2 pulsanti*/
    GtkWidget *hbox=gtk_hbox_new(TRUE, 6);
    gtk_container_add (GTK_CONTAINER (vbox), hbox);
    GtkWidget *pulsante1=gtk_button_new_with_label(str_p1);
    gtk_box_pack_start (GTK_BOX (hbox), pulsante1, TRUE, TRUE, 0);
    GtkWidget *pulsante2=gtk_button_new_with_label(str_p2);
    gtk_box_pack_start (GTK_BOX (hbox), pulsante2, TRUE, TRUE, 0);
    /**Creo una hbox in cui inserisco altri 2 pulsanti*/
    GtkWidget *hbox_bottom=gtk_hbox_new(TRUE, 6);
    gtk_container_add (GTK_CONTAINER (vbox), hbox_bottom);
    GtkWidget *pulsante3=gtk_button_new_with_label(str_p3);
    gtk_box_pack_start (GTK_BOX (hbox_bottom), pulsante3, TRUE, TRUE, 0);
    GtkWidget *pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (hbox_bottom), pulsante_indietro, TRUE, TRUE, 0);


    /**Collego delle funzioni alla pressione dei 4 tasti*/
    g_signal_connect(G_OBJECT(pulsante1), "clicked", G_CALLBACK(scelta_ordinazione), scontrini);                        ///Invoco la procedura che mi permette di prendere un'ordinazione
    g_signal_connect(G_OBJECT(pulsante2), "clicked", G_CALLBACK(stampa_scontrino), scontrini);                          ///Invoco la procedura che mi permette di stampare lo scontrino
    g_signal_connect(G_OBJECT(pulsante3), "clicked", G_CALLBACK(visualizza_ordinazioni), scontrini);                    ///Invoco la procedura che mi permette di visualizzare gli ordini di un tavolo
    g_signal_connect(G_OBJECT(pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)finestra_ordinazioni);    ///Invoco la procedura che mi permette di tornare alla finestra precedente

    g_signal_connect(G_OBJECT(finestra_ordinazioni), "destroy", G_CALLBACK(aggiorna), (gpointer)scontrini);             ///Quando questa finestra viene chiusa, riapro la finestra di gestione della cassa, in questo modo il contenuto della finestra si mantiene sempre aggiornato

    gtk_widget_show_all(finestra_ordinazioni);                                                                          ///Rendo il tutto visibile

}

void scelta_ordinazione(GtkWidget *pulsante_operazione, Scontrino scontrini[N_TAVOLI])      ///Finestra adibita alla scelta di un piatto da ordinare (**Contiene delle parti di core**)
{
    FILE *fp=NULL;                      ///Dichiaro un puntatore a file che mi servir� per leggere i piatti dal menu. (N.B: Il vettore di piatti non � visibile in questa funzione per colpa del passaggio di parametri in GTK!!)
    Piatto buffer;                      ///Dichiaro una variabile di tipo Piatto che mi servir� per stampare ogni singolo piatto
    GtkWidget *button;                  ///Dichiaro un puntatore a GtkWidget
    char stringa[500], str_temp[40];    ///Dichiaro due stringhe che mi serviranno per stampare dei pulsanti e leggere dei numeri
    const gchar *label_temp;            ///Dichiaro una variabile di tipo gchar che mi servir� per eseguire correttamente l'istruzione gtk_button_get_label
    int numero_tavolo;                  ///Diciaro una variabile nella quale salver� l'id del tavolo

    /**Leggo il testo scritto nel pulsante premuto dall'utente ed estraggo da esso l'id del tavolo da servire*/
    label_temp=gtk_button_get_label((GtkButton*)pulsante_operazione);
    printf("NEL PULSANTE C'E' SCRITTO %s\n", label_temp);
    strcpy(str_temp, label_temp);
    sscanf(str_temp, "Tavolo %d: prendi ordinazione", &numero_tavolo);

    /**Creo una finestra nella quale inserisco una vbox*/
    GtkWidget *finestra=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (finestra), "Gestione Ordinazioni");
    gtk_window_set_default_size(GTK_WINDOW(finestra), 1550, 800);
    gtk_window_set_position (GTK_WINDOW (finestra), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (finestra) ,15);
    GtkWidget *vbox_main=gtk_vbox_new(FALSE, 6);
    gtk_container_add (GTK_CONTAINER (finestra), vbox_main);

    /**Inserisco un'icona, una label ed una finestra scrolled dentro la vbox*/
    GtkWidget *icona=gtk_image_new_from_file("Menu.png");
    gtk_box_pack_start (GTK_BOX (vbox_main), icona, FALSE, FALSE, 0);
    GtkWidget *label=gtk_label_new("Scegli un piatto tra i seguenti:");
    gtk_box_pack_start (GTK_BOX (vbox_main), label, FALSE, FALSE, 0);
    GtkWidget *finestra_scrolled=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(vbox_main), finestra_scrolled);

    /**Inserisco una vbox dentro la finestra scrolled*/
    GtkWidget *vbox=gtk_vbox_new(FALSE, 6);
    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (finestra_scrolled), vbox);

    /**Dichiaro una serie di frame e hbox (ogni hbox verr� inserita nel frame corrispondente*/
    GtkWidget *frame_antipasti=NULL, *hbox_antipasti=NULL, *frame_primi=NULL, *hbox_primi=NULL, *frame_secondi=NULL;
    GtkWidget *hbox_secondi=NULL, *frame_contorni=NULL, *hbox_contorni=NULL, *frame_dessert=NULL, *hbox_dessert=NULL;

    /**Personalizzo tutti i frame e inserisco tutte le hbox dentro il frame corrispondente*/
    frame_antipasti=gtk_frame_new("\n----------ANTIPASTI----------");
    gtk_frame_set_label_align((GtkFrame*)frame_antipasti, 0.5, 0.8);
    gtk_box_pack_start (GTK_BOX (vbox), frame_antipasti, TRUE, TRUE, 0);
    hbox_antipasti=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_antipasti), hbox_antipasti);

    frame_primi=gtk_frame_new("\n----------PRIMI----------");
    gtk_frame_set_label_align((GtkFrame*)frame_primi, 0.5, 0.8);
    gtk_box_pack_start (GTK_BOX (vbox), frame_primi, TRUE, TRUE, 0);
    hbox_primi=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_primi), hbox_primi);

    frame_secondi=gtk_frame_new("\n----------SECONDI----------");
    gtk_frame_set_label_align((GtkFrame*)frame_secondi, 0.5, 0.8);
    gtk_box_pack_start (GTK_BOX (vbox), frame_secondi, TRUE, TRUE, 0);
    hbox_secondi=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_secondi), hbox_secondi);

    frame_contorni=gtk_frame_new("\n----------CONTORNI----------");
    gtk_frame_set_label_align((GtkFrame*)frame_contorni, 0.5, 0.8);
    gtk_box_pack_start (GTK_BOX (vbox), frame_contorni, TRUE, TRUE, 0);
    hbox_contorni=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_contorni), hbox_contorni);

    frame_dessert=gtk_frame_new("\n----------DESSERT----------");
    gtk_frame_set_label_align((GtkFrame*)frame_dessert, 0.5, 0.8);
    gtk_box_pack_start (GTK_BOX (vbox), frame_dessert, TRUE, TRUE, 0);
    hbox_dessert=gtk_hbox_new(FALSE, 6);
    gtk_container_add(GTK_CONTAINER(frame_dessert), hbox_dessert);

    fp=fopen("piatti.dat", "rb");                                   ///Apro il file dei piatti. (N.B: Il vettore dei piatti non � visibile in questa funzione per via del metodo di passaggio di parametri nelle GTK)
    if(fp==NULL || fread(&buffer, sizeof(Piatto), 1, fp)==0)        ///Se il file non esiste avviso l'utente e torno alla finestra precedente
    {
        printf("Errore nell'apertura del file piatti.dat\n");
        avvisa(2);
        gtk_widget_destroy(finestra);
    }
    rewind(fp);
    while(fread(&buffer, sizeof(Piatto), 1, fp)==1)                 ///Man mano che leggo il file memorizzo ogni piatto nella variabile buffer
    {
        switch(buffer.tipologia_piatto)                             ///Suddivido i piatti letti in base alla tipologia
        {
            case 0:
                //sprintf(stringa, "[Tavolo %d]\nPiatto #%d: %s, (Tipologia %d )\n %s,%s,%s\n%.2f", numero_tavolo, buffer.id_piatto, buffer.nome_piatto, buffer.tipologia_piatto, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
                stampa_pulsante(stringa, numero_tavolo, buffer);    ///Invoco la funzione che mi permette di stampare la stringa contenente la pietanza
                //sprintf(stringa, "[Tavolo %d]\nPiatto #%d:\n%s\n(Tipologia %d )\n%s,%s,%s\nprezzo: %.2f", numero_tavolo, buffer.id_piatto, buffer.nome_piatto, buffer.tipologia_piatto, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
                button=gtk_button_new_with_label(stringa);          ///Creo un pulsante contenente la stampa del piatto appena avvenuta..collego poi al pulsante la funzione che mi permette di ordinare quel piatto
                gtk_box_pack_start (GTK_BOX (hbox_antipasti), button, FALSE, FALSE, 0);
                g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(prendi_ordinazione), (gpointer)scontrini);
            break;
                        ///Idem per le altre tipologie...
            case 1:
                //sprintf(stringa, "[Tavolo %d]\nPiatto #%d: %s, (Tipologia %d )\n %s,%s,%s\n%.2f", numero_tavolo, buffer.id_piatto, buffer.nome_piatto, buffer.tipologia_piatto, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
                stampa_pulsante(stringa, numero_tavolo, buffer);
                button=gtk_button_new_with_label(stringa);
                gtk_box_pack_start (GTK_BOX (hbox_primi), button, FALSE, FALSE, 0);
                g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(prendi_ordinazione), (gpointer)scontrini);
            break;
            case 2:
                //sprintf(stringa, "[Tavolo %d]\nPiatto #%d: %s, (Tipologia %d )\n %s,%s,%s\n%.2f", numero_tavolo, buffer.id_piatto, buffer.nome_piatto, buffer.tipologia_piatto, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
                stampa_pulsante(stringa, numero_tavolo, buffer);
                button=gtk_button_new_with_label(stringa);
                gtk_box_pack_start (GTK_BOX (hbox_secondi), button, FALSE, FALSE, 0);
                g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(prendi_ordinazione), (gpointer)scontrini);
            break;
            case 3:
                //sprintf(stringa, "[Tavolo %d]\nPiatto #%d: %s, (Tipologia %d )\n %s,%s,%s\n%.2f", numero_tavolo, buffer.id_piatto, buffer.nome_piatto, buffer.tipologia_piatto, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
                stampa_pulsante(stringa, numero_tavolo, buffer);
                button=gtk_button_new_with_label(stringa);
                gtk_box_pack_start (GTK_BOX (hbox_contorni), button, FALSE, FALSE, 0);
                g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(prendi_ordinazione), (gpointer)scontrini);
            break;
            case 4:
                //sprintf(stringa, "[Tavolo %d]\nPiatto #%d: %s, (Tipologia %d )\n%s,%s,%s\n%.2f", numero_tavolo, buffer.id_piatto, buffer.nome_piatto, buffer.tipologia_piatto, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
                stampa_pulsante(stringa, numero_tavolo, buffer);
                button=gtk_button_new_with_label(stringa);
                gtk_box_pack_start (GTK_BOX (hbox_dessert), button, FALSE, FALSE, 0);
                g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(prendi_ordinazione), (gpointer)scontrini);
            break;
            default: printf("Errore switch-case\n");
            break;
        }
    }

    /**Creo un pulsante che mi permette di tornare alla finestra precedente*/
    GtkWidget *pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox_main), pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)finestra);
    gtk_widget_show_all(finestra);
    fclose(fp);        ///Chiudo il file
}

void visualizza_ordinazioni(GtkWidget *pulsante, Scontrino scontrini[N_TAVOLI])             ///Finestra di visualizzazione delle ordinazioni correnti di un tavolo
{
    printf("Ok, visualizza ordinazioni\n");
    char str_temp[50], str_piatto[200];                                                     ///Dichiaro due stringhe che mi serviranno per diversi motivi
    const gchar *label_temp;                                                                ///Variabile gchar che mi serve affinch� gtk_button_get_label vada a buon fine
    int numero_tavolo, num_ordinazioni, i;                                                  ///Due variabili in cui salver� l'id del tavolo ed il numero di ordinazioni...+ contatore
    label_temp=gtk_button_get_label((GtkButton*)pulsante);                                  ///Leggo il testo scritto nel pulsante ed estraggo da esso l'id del tavolo, che salvo nella variabile apposita
    printf("NEL PULSANTE C'E' SCRITTO %s\n", label_temp);
    strcpy(str_temp, label_temp);
    sscanf(str_temp, "Tavolo %d: visualizza ordinazioni", &numero_tavolo);
    sprintf(str_temp, "Il tavolo %d ha ordinato le seguenti pietanze:", numero_tavolo);
    printf("\nIL NUMERO DEL TAVOLO E' %d\n", numero_tavolo);
    if(scontrini[numero_tavolo-1].id_tavolo==-1)                                            ///Se il tavolo non ha ancora ordinato nulla, avviso ed esco
    {
        avvisa(1);
        return;
    }
    num_ordinazioni=scontrini[numero_tavolo-1].num_ordinazioni;                             ///Trasferisco per comodit� il numero di ordinazioni in una variabile

    /**Creo una finestra e inserisco all'interno una vbox*/
    GtkWidget *finestra=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (finestra), "Visualizza Ordinazioni");
    gtk_window_set_default_size(GTK_WINDOW(finestra), 500, 780);
    gtk_window_set_position (GTK_WINDOW (finestra), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (finestra) ,15);
    GtkWidget *vbox_main=gtk_vbox_new(FALSE, 6);
    gtk_container_add (GTK_CONTAINER (finestra), vbox_main);

    /**Inserisco un'icona, una label e una finestra scrolled dentro la vbox*/
    GtkWidget *icona=gtk_image_new_from_file("Torta.png");
    gtk_box_pack_start (GTK_BOX (vbox_main), icona, FALSE, FALSE, 0);
    GtkWidget *label=gtk_label_new(str_temp);
    gtk_box_pack_start (GTK_BOX (vbox_main), label, FALSE, FALSE, 0);
    GtkWidget *finestra_scrolled=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(vbox_main), finestra_scrolled);

    /**Inserisco una vbox dentro la finestra scrolled*/
    GtkWidget *vbox=gtk_vbox_new(FALSE, 6);
    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (finestra_scrolled), vbox);

    /**Creo un pulsante che mi permette di tornare alla finestra precedente*/
    GtkWidget *pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox_main), pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)finestra);

    /**Dichiaro un puntatore di tipo GtkWidget che diventer� la label che conterr� i piatti*/
    GtkWidget *label_piatto;

    for(i=0; i<num_ordinazioni; i++)    ///Faccio scorrere tutte le ordinazioni del tavolo
    {
        ///Stampo l'ordinazione corrente in una stringa
        sprintf(str_piatto, "\n%d x %s", scontrini[numero_tavolo-1].ordini[i].quantita,
                                         scontrini[numero_tavolo-1].ordini[i].corrente.nome_piatto);
        ///Copio la stringa nella label
        label_piatto=gtk_label_new(str_piatto);
        ///Inserisco la label dentro la vbox della scrolled window
        gtk_box_pack_start (GTK_BOX (vbox), label_piatto, FALSE, FALSE, 0);
    }
    ///Rendo il tutto visibile
    gtk_widget_show_all(finestra);
}

void stampa_scorte(GtkWidget *button, Scontrino scontrini[N_TAVOLI])                        ///Finestra di visualizzazione delle scorte utilizzate
{
    printf("Ok gestione scorte\n");
    FILE *fp=NULL;      ///Dichiaro un puntatore a file,
    char stringa[70];   ///una stringa
    Scorta buffer;      ///e una variabile di tipo Scorta

    /**Creo una finestra dentro la quale inserisco una vbox*/
    GtkWidget *win=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (win), "Gestione Scorte");
    gtk_window_set_default_size(GTK_WINDOW(win), 1000, 800);
    gtk_window_set_position (GTK_WINDOW (win), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (win) ,15);
    GtkWidget *vbox_main=gtk_vbox_new(FALSE, 6);
    gtk_container_add (GTK_CONTAINER (win), vbox_main);

    /**Creo una hbox all'interno della quale inserisco un'immagine*/
    GtkWidget *hbox_icona=gtk_hbox_new(FALSE, 6);
    gtk_box_pack_start (GTK_BOX (vbox_main), hbox_icona, FALSE, FALSE, 0);
    GtkWidget *icona=gtk_image_new_from_file("scorte.png");
    gtk_box_pack_start (GTK_BOX (hbox_icona), icona, TRUE, TRUE, 0);

    /**Inserisco una label e una scrolled window dentro la vbox*/
    GtkWidget *label=gtk_label_new("Al momento hai utilizzato le seguenti scorte:");
    gtk_box_pack_start (GTK_BOX (vbox_main), label, FALSE, FALSE, 0);
    GtkWidget *finestra_scrolled=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(vbox_main), finestra_scrolled);

    /**Inserisco una vbox dentro la scrolled window*/
    GtkWidget *vbox=gtk_vbox_new(FALSE, 6);
    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (finestra_scrolled), vbox);

    /**Inserisco due pulsanti dentro la vbox principale*/
    GtkWidget *p_elimina=gtk_button_new_with_label("\nElimina l'intero file delle scorte\n");
    gtk_box_pack_start (GTK_BOX (vbox_main), p_elimina, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(p_elimina), "clicked", G_CALLBACK(elimina_scorte), (gpointer)win);    ///Invoco la procedura che mi permette di eliminare il file delle scorte
    GtkWidget *pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox_main), pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)win); ///Invoco la procedura che mi permette di tornare alla finestra precedente

    fp=fopen("scorte.dat", "rb");                                           ///Apro il file delle scorte in modalit� di sola lettura
    while(fread(&buffer, sizeof(Scorta), 1, fp)==1)                         ///Leggo il file e memorizzo ogni Scorta nella variabile buffer
    {
        sprintf(stringa, "%s x %d\n", buffer.nome, buffer.quantita);        ///Salvo all'interno di una stringa la scorta
        printf("\nnome=%s, quantita=%d\n", buffer.nome, buffer.quantita);
        label=gtk_label_new(stringa);                                       ///Creo una label a partire dalla stringa
        gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);          ///Inserisco la label dentro la vbox
    }
    fclose(fp);                                                             ///Chiudo il file

    gtk_widget_show_all(win);                                               ///Rendo il tutto visibile
}

void elimina_scorte(GtkWidget *button, GtkWidget *win)                                      ///Da qui � possibile eliminare il file delle scorte
{
    remove("scorte.dat");       ///Elimino il file delle scorte
    /**Creo una finestra di dialogo per avvisare*/
    GtkWidget *dialog=gtk_message_dialog_new (GTK_WINDOW (win),GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, "Il file delle scorte e' stato eliminato correttamente!");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
    gtk_widget_destroy(win);    ///Chiudo la finestra delle scorte
}

/***** SPAZIO DEDICATO ALLE PROCEDURE AUSILIARIE (procedure che solitamente vengono richiamate all'interno di altre funzioni) *****/

void stampa_pietanza(Piatto buffer, GtkWidget *label, char stringa[NOME], GtkWidget *hbox)  ///Procedura ausiliaria della funzione stampa, serve per evitare ripetizioni inutili nel codice
{
    /**Stampo una stringa contenente la pietanza*/
    sprintf(stringa, "\n\tPiatto #%d\n\t%s\n\tIngredienti:\n\t%s, %s, %s\n\tPrezzo: %.2f\n", buffer.id_piatto, buffer.nome_piatto, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
    /**Creo una label a partire dalla stringa*/
    label=gtk_label_new(stringa);
    /**Inserisco la label dentro l'hbox giusta*/
    gtk_container_add (GTK_CONTAINER (hbox), label);
}

void stampa_pulsante(char stringa[500], int numero_tavolo, Piatto buffer)                   ///Procedura simile alla precedente, vedi funzione "scelta_ordinazione"
{
    //sprintf(stringa, "[Tavolo %d]\nPiatto #%d:\n%s\n(Tipologia %d )\n%s,%s,%s\nprezzo: %.2f", numero_tavolo, buffer.id_piatto, buffer.nome_piatto, buffer.tipologia_piatto, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
    /**Stampo la stringa corrispondente alla pietanza*/
    sprintf(stringa, "[Tavolo %d] Piatto #%d:\n\n%s\n(Tipologia %d)\nIngredienti: %s, %s, %s\nPrezzo: %.2f", numero_tavolo, buffer.id_piatto, buffer.nome_piatto, buffer.tipologia_piatto, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
}

void distruggi(GtkWidget *pulsante, GtkWidget *finestra)                                    ///Procedura che serve per chiudere una finestra precedentemente aperta
{
    /**Chiudo la finestra*/
    gtk_widget_destroy(finestra);
}

void avvisa(int parametro)                                                                  ///Procedura che serve a creare una finestra simile a quella di dialogo
{
    /**Creo una finestra e inserisco all'interno una vbox*/
    GtkWidget *win=NULL;
    win=gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_container_set_border_width (GTK_CONTAINER (win), 8);
    gtk_window_set_title (GTK_WINDOW (win), "");
    gtk_window_set_default_size(GTK_WINDOW(win), 250, 250);
    gtk_window_set_position (GTK_WINDOW (win), GTK_WIN_POS_CENTER);
    GtkWidget *vbox = gtk_vbox_new (FALSE, 6);
    gtk_container_add (GTK_CONTAINER (win), vbox);
    /**Dichiaro un puntatore a GtkWidget che diventer� la label da inserire nella finestra di avviso*/
    GtkWidget *label;
    /**Stampo una label diversa a seconda del parametro che viene passato in ingresso*/
    switch(parametro)
    {
        case 0: label=gtk_label_new("\nPrenotazione effettuata!\n");
                break;
        case 1: label=gtk_label_new("\nIl tavolo non ha ancora ordinato alcun piatto!\n");
                break;
        case 2: label=gtk_label_new("\nIl menu non contiene alcun piatto!\n");
                break;
        case 3: label=gtk_label_new("\nIl nuovo nome inserito e' gia' presente nel menu!\n");
                break;
        case 4: label=gtk_label_new("\nIl nome cercato non compare nel menu!\n");
                break;
        case 5: label=gtk_label_new("\nScrittura avvenuta con successo!\n");
                break;
        case 6: label=gtk_label_new("\nEliminazione avvenuta con successo!\n");
                break;
    }
    /**Inserisco la label dentro la vbox*/
    gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
    /**Creo un pulsante che mi permette di chiudere la finestra corrente*/
    GtkWidget *button=gtk_button_new_from_stock(GTK_STOCK_OK);
    gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 0);
    g_signal_connect (button, "clicked", G_CALLBACK(distruggi), win);
    /**Se la finestra viene messa in secondo piano faccio in modo che venga chiusa automaticamente*/
    g_signal_connect (win, "focus-out-event", G_CALLBACK(gtk_widget_hide), NULL);
    /**Rendo il tutto visibile*/
    gtk_widget_show_all(win);
}

void pulsanti_radio(Elementigtk *pacchetto, GtkWidget *box_tipologie)                       ///Procedura che serve a creare dei pulsanti radio
{
    /**Creo 5 pulsanti radio, uno per ogni tipologia di piatto.
       Li inserisco dentro l'hbox delle tipologie.
       Collego ad ogni pulsante la funzione che permette di prenotare la tipologia di piatto corrispondente.
    **/
    pacchetto->p_antipasto=gtk_radio_button_new_with_label(NULL, "0)Antipasto");
    gtk_box_pack_start(GTK_BOX(box_tipologie), pacchetto->p_antipasto, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->p_antipasto), "clicked", G_CALLBACK(prenota_pasto), (gpointer)pacchetto);
    /**Creo un gruppo per i pulsanti radio*/
    GSList *gruppo=gtk_radio_button_group( (GtkRadioButton*) pacchetto->p_antipasto );
    /**Imposto il primo pulsante come attivo*/
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (pacchetto->p_antipasto), TRUE);

    /**Idem per tutti i pulsanti successivi...*/
    pacchetto->p_primo=gtk_radio_button_new_with_label(gruppo, "1)Primo");
    gruppo=gtk_radio_button_group( (GtkRadioButton*) pacchetto->p_primo );
    gtk_box_pack_start(GTK_BOX(box_tipologie), pacchetto->p_primo, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->p_primo), "clicked", G_CALLBACK(prenota_pasto), (gpointer)pacchetto);

    pacchetto->p_secondo=gtk_radio_button_new_with_label(gruppo, "2)Secondo");
    gruppo=gtk_radio_button_group( (GtkRadioButton*) pacchetto->p_secondo );
    gtk_box_pack_start(GTK_BOX(box_tipologie), pacchetto->p_secondo, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->p_secondo), "clicked", G_CALLBACK(prenota_pasto), (gpointer)pacchetto);

    pacchetto->p_contorno=gtk_radio_button_new_with_label(gruppo, "3)Contorno");
    gruppo=gtk_radio_button_group( (GtkRadioButton*) pacchetto->p_contorno );
    gtk_box_pack_start(GTK_BOX(box_tipologie), pacchetto->p_contorno, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->p_contorno), "clicked", G_CALLBACK(prenota_pasto), (gpointer)pacchetto);

    pacchetto->p_dessert=gtk_radio_button_new_with_label(gruppo, "4)Dessert");
    gruppo=gtk_radio_button_group( (GtkRadioButton*) pacchetto->p_dessert );
    gtk_box_pack_start(GTK_BOX(box_tipologie), pacchetto->p_dessert, TRUE, TRUE, 0);
    g_signal_connect(G_OBJECT(pacchetto->p_dessert), "clicked", G_CALLBACK(prenota_pasto), (gpointer)pacchetto);
}

void refresh(GtkWidget *finestra, Scontrino *scontrini)                                     ///Procedura che mi serve per chiudere la finestra di gestione cassa, viene invocata per aggiornare il contenuto della finestra (solo in guesto caso la funzione "distruggi" non � adatta)
{
    /**Chiudo la finestra passata in ingresso*/
    gtk_widget_destroy(finestra);
}

void aggiorna(GtkWidget *finestra, Scontrino *scontrini)                                    ///Procedura che viene invocata dopo la precedente, si occupa di richiamare la funzione gestione_cassa al fine di aggiornare il contenuto della finestra
{
    /**Invoco nuovamente la funzione di gestione della cassa*/
    gestione_cassa(NULL, scontrini);
}
