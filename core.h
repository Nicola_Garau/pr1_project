/**In questo file ho deciso di inserire tutte le definizioni di costanti, enumerazioni, strutture e funzioni riguardanti
   principalmente il core del programma. Tutte le dichiarazioni di funzioni relative all'interfaccia grafica sono reperibili nel file gui.h*/

#ifndef CORE_HEADERS    ///Mi assicuro che il file venga letto soltanto una volta, per evitare un loop di inclusioni
#define CORE_HEADERS

#include <stdio.h>      ///Input-output
#include <stdlib.h>     ///Utilit� generale
#include <string.h>     ///Manipolazione di stringhe
#include <gtk/gtk.h>    ///Interfaccia
#include <ctype.h>      ///Libreria che mi serve per usare la funzione "tolower" al fine di rendere il programma case-insensitive

///SPAZIO COSTANTI

#define NOME 101        ///Costante che indica la lunghezza massima del nome di un piatto
#define N_ING 3         ///Costante che indica il numero massimo di ingredienti per piatto
#define N_TAVOLI 15     ///Costante che indica il numero dei tavoli
#define SCONTO 10       ///Costante che indica il valore dello sconto applicabile

///FINE SPAZIO COSTANTI

///SPAZIO ENUMERAZIONI

/** Tipologia del piatto: 5 scelte possibili, definite dalla seguente enumerazione **/
typedef enum {antipasto,primo,secondo,contorno,dessert} Tipologie;

///FINE SPAZIO ENUMERAZIONI

///SPAZIO STRUTTURE

typedef struct{                                 ///Struttura contenente le informazioni relative ad un piatto
    int id_piatto;                              ///id del piatto: assegnato in maniera progressiva
    char nome_piatto[NOME];                     ///nome del piatto: lunghezza massima definita da NOME
    Tipologie tipologia_piatto;                 ///tipologia del piatto: definita come tipo enumerativo dichiarato in precedenza (Tipologie)
    char ingredienti_piatto[N_ING][NOME];       ///ingredienti del piatto: numero massimo di ingredienti definito da N_ING, lunghezza massima definita da NOME
    float prezzo_piatto;                        ///prezzo del piatto: definito da una costante di tipo reale
} Piatto;                                       ///Chiamo la struttura appena creata "Piatto"

typedef struct{                                 ///Struttura contenente le informazioni relative ad un ordine
    Piatto corrente;                            ///variabile di tipo Piatto: contiene tutte le informazioni del piatto ordinato
    int quantita;                               ///quantit�: indica quante porzioni del piatto corrente sono state ordinate dal tavolo corrente
} Ordine;                                       ///Chiamo la struttura appena creata "Ordine"

typedef struct{                                 ///Struttura contenente le informazioni relative ad uno scontrino
    int id_tavolo;                              ///id del tavolo: indica il numero d'identificazione del tavolo servito
    Ordine *ordini;                             ///puntatore degli ordini di tipo Ordine: allocato dinamicamente, contiene tutti i piatti ordinati dal tavolo corrente
    float prezzo_totale;                        ///prezzo totale: variabile reale che viene inizializzata durante la stampa dello scontrino, contiene la somma degli importi
    int num_ordinazioni;                        ///numero ordinazioni: conta le ordinazioni SOLO di piatti diversi (se viene ordinato due volte lo stesso piatto, num_ordinazioni varr� 1)
} Scontrino;                                    ///Chiamo la struttura appena creata "Scontrino"

typedef struct{                                 ///Struttura contenente le informazioni relative ad una scorta
    char nome[NOME];                            ///nome della scorta: lunghezza massima definita da NOME
    int quantita;                               ///quantit�: indica quante volte la scorta corrente � stata utilizzata
} Scorta;                                       ///Chiamo la struttura appena creata "Scorta"

typedef struct{                                 ///Forse la struttura pi� importante, mi permette di gestire tutti gli elementi dell'interfaccia grafica e non solo
    GtkWidget *finestra_principale;             ///Elementi relativi alla finestra principale
    GtkWidget *pulsante_menu;
    GtkWidget *pulsante_cassa;
    GtkWidget *pulsante_scorte;
    GtkWidget *pulsante_esci;

    GtkWidget *finestra_menu;                   ///Elementi relativi alla finestra della gestione del menu
    GtkWidget *pulsante_aggiungi;
    GtkWidget *pulsante_modifica;
    GtkWidget *pulsante_elimina;
    GtkWidget *pulsante_stampa;
    GtkWidget *pulsante_ricerca;
    GtkWidget *pulsante_indietro;

    GtkWidget *finestra_aggiungi_piatto;        ///Elementi relativi alla finestra di aggiunta di un piatto
    GtkWidget *nome;
    GtkWidget *p_antipasto;
    GtkWidget *p_primo;
    GtkWidget *p_secondo;
    GtkWidget *p_contorno;
    GtkWidget *p_dessert;
    GtkWidget *tipologia;
    GtkWidget *ingrediente1;
    GtkWidget *ingrediente2;
    GtkWidget *ingrediente3;
    GtkWidget *prezzo;
    GtkWidget *pulsante_ok;
    GtkWidget *dialog;

    GtkWidget *finestra_modifica_piatto;        ///Elementi relativi alla finestra di modifica di un piatto
    GtkWidget *entry;
    GtkWidget *e_nome;
    GtkWidget *e_tipologia;
    GtkWidget *e_ing1;
    GtkWidget *e_ing2;
    GtkWidget *e_ing3;
    GtkWidget *e_prezzo;
    GtkWidget *p_tip;

    Tipologie tip_enum;                         ///Variabile di tipo "Tipologie" che mi serve per far funzionare la procedura "prenota_pasto"

    GtkWidget *finestra_elimina_piatto;         ///Finestra di eliminazione dei piatti
    GtkWidget *finestra_stampa;                 ///Finestra di stampa dei piatti
    GtkWidget *finestra_scrolled;               ///Scrolled window, usata pi� volte nel programma
    GtkWidget *finestra_ricerca;                ///Finestra di ricerca dei piatti

    Scontrino *punt_scontrini;                  ///Puntatore al vettore di scontrini: mi serve per estendere la visibilit� del vettore all'interno del programma, in particolare per far funzionare la procedura "controlla_se_modificabile"
    int n_tavolo;                               ///Variabile che mi serve per tener conto del tavolo su cui sto lavorando
    Piatto *vettore;                            ///Vettore contenente tutti i piatti del menu: mi permette di evitare troppi accessi al file dei piatti
    int dim_vettore;                            ///Variabile che mi serve per tener conto del numero di piatti presenti nel menu

}   Elementigtk;                                ///Chiamo la struttura appena creata "Elementigtk"

///FINE SPAZIO STRUTTURE


///SPAZIO FUNZIONI


/** PROCEDURE/FUNZIONI AUSILIARIE PRESENTI NEL FILE core.c (vedi fine del file) **/
void carica_piatti(Elementigtk *pacchetto);                                                     ///carica nel vettore di piatti i piatti presenti nel file dei piatti
void prenota_pasto(GtkWidget *pulsante, Elementigtk *pacchetto);                                ///prenoto una tipologia di piatto utilizzando la variabile tip_enum definita nella struttura Elementigtk
void carica_scorte(Elementigtk *pacchetto);                                                     ///carica nel file delle scorte tutti gli ingredienti presenti nel vettore dei piatti, nel caso non ci fossero gi�
void inserisci_scorte(Piatto corrente);                                                         ///aggiorna le scorte all'aggiunta di un piatto
void aggiorna_scorte(int num_ordinazioni, int numero_tavolo, Scontrino scontrini[N_TAVOLI]);    ///aggiorna le scorte alla stampa di uno scontrino
void fai_il_conto(Scontrino scontrini[N_TAVOLI], int numero_tavolo, FILE *fp, Tipologie tipo);  ///calcola l'importo relativo ad un'ordinazione e lo stampa su stringa assieme ad altre informazioni
int controlla_se_modificabile(Elementigtk *pacchetto, Piatto risultato, GtkWidget *win);        ///controlla se un piatto pu� essere modificato (se non � stato ordinato da nessun tavolo)
Piatto controlla_nome(GtkWidget *entry, Elementigtk *pacchetto);                                ///controlla se il nome di un piatto � gi� presente o meno nel menu
void dealloca(Scontrino *scontrini);                                                            ///dealloca la memoria allocata e termina il processo gtk

/** PROCEDURE PRESENTI NEL FILE core.c **/
void inserisci_primo_piatto(GtkWidget *pulsante, Elementigtk *pacchetto);                       ///inserisce il primo piatto all'interno del menu
void inserisci_piatto(GtkWidget *pulsante, Elementigtk *pacchetto);                             ///inserisce un piatto all'interno del menu
void modifica_nome(GtkWidget *pulsante, Elementigtk *pacchetto);                                ///modifica il nome di un piatto
void modifica_tipologia(GtkWidget *pulsante, Elementigtk *pacchetto);                           ///modifica la tipologia di un piatto
void modifica_ingrediente(GtkWidget *pulsante, Elementigtk *pacchetto);                         ///modifica un ingrediente di un piatto
void modifica_prezzo(GtkWidget *pulsante, Elementigtk *pacchetto);                              ///modifica il prezzo di un piatto
void elimina_un_piatto(GtkWidget *pulsante, Elementigtk *pacchetto);                            ///elimina un piatto
void elimina_tutto(GtkWidget *pulsante, Elementigtk *pacchetto);                                ///elimina tutti i piatti
void ricerca_per_ingrediente(GtkWidget(*pulsante_modifica), Elementigtk *pacchetto);            ///ordina un piatto per un tavolo
void prendi_ordinazione(GtkWidget *pulsante_operazione, Scontrino scontrini[N_TAVOLI]);         ///stampa lo scontrino per un tavolo
void stampa_scontrino(GtkWidget *pulsante, Scontrino scontrini[N_TAVOLI]);                      ///calcola e stampa il totale degli scontrini andati in stampa
void totale_scontrini(float importo_totale);


///FINE SPAZIO FUNZIONI

#endif
