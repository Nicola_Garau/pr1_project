/**
    In questo file � possibile trovare tutte le definizioni delle funzioni riguardanti il core del programma.
    (Tuttavia � possibile trovare alcuni pezzi di codice che riguardano anche l'interfaccia).
    Si consiglia di compattare ogni funzione per avere una visione generale di come funziona il programma.
 **/

#include "core.h"   ///Includo il file header core.h nel quale sono contenute tutte le dichiarazioni di funzioni, inclusioni di libreria, ecc. riguardanti il core del programma
#include "gui.h"    ///ed il file gui.h che contiene delle direttive necessarie per far funzionare correttamente l'interfaccia grafica


void inserisci_primo_piatto(GtkWidget *pulsante, Elementigtk *pacchetto)                        ///Con questa procedura viene inserito il primo piatto sia all'interno del file dei piatti che del vettore corrispondente
{
    Piatto corrente;            ///Dichiaro una variabile d'appoggio di tipo Piatto per una questione di semplicit� nella gestione del codice
    corrente.id_piatto=1;       ///Inizializzo l'id del piatto a 1 perch� � il primo piatto che viene inserito
    FILE *fp=NULL;              ///Dichiaro un puntatore a file
    size_t dim;                 ///..e una variabile di tipo size_t con la quale verificher� l'esito di un'allocazione
    char str_appoggio[NOME];    ///Dichiaro una stringa d'appoggio in modo che l'istruzione gtk_entry_get_text vada sempre a buon fine (� un modo per evitare tutti gli errori di conversione che ho incontrato)
    int i;                      ///Dichiaro un contatore

    /**Ho deciso di usare una variabile temporanea di tipo Piatto per rimuovere tutti gli eventuali problemi di conversione
       dovuti alle entry GTK e soprattutto per poter inserire facilmente dei controlli sulle lettere maiuscole/minuscole */

    strcpy(corrente.nome_piatto ,gtk_entry_get_text(GTK_ENTRY(pacchetto->nome)));          ///Acquisisco il nome del piatto dalla entry corrispondente e lo copio nel campo "nome_piatto"
    corrente.tipologia_piatto=(Tipologie)pacchetto->tip_enum;                              ///Copio la tipologia, gi� "prenotata" grazie alla funzione prenota_pasto
    strcpy(str_appoggio, gtk_entry_get_text(GTK_ENTRY(pacchetto->ingrediente1)));          ///Copio dalla entry l'ingrediente 1,
    strcpy(corrente.ingredienti_piatto[0], str_appoggio);
    strcpy(str_appoggio, gtk_entry_get_text(GTK_ENTRY(pacchetto->ingrediente2)));          ///...il 2,
    strcpy(corrente.ingredienti_piatto[1] ,str_appoggio);
    strcpy(str_appoggio, gtk_entry_get_text(GTK_ENTRY(pacchetto->ingrediente3)));          ///...ed il 3
    strcpy(corrente.ingredienti_piatto[2] ,str_appoggio);
    strcpy(str_appoggio, gtk_entry_get_text(GTK_ENTRY(pacchetto->prezzo)));                ///Copio nella stringa d'appoggio il testo presente nella entry del prezzo
    sscanf(str_appoggio, "%f", &corrente.prezzo_piatto);                                   ///Leggo un numero dalla stringa e lo assegno al campo prezzo del piatto corrente

    /**Faccio in modo che tutte le stringhe degli ingredienti siano scritte in minuscolo, per comodit� e per una questione estetica*/
        for(i=0; i<strlen(corrente.ingredienti_piatto[0]); i++)
            corrente.ingredienti_piatto[0][i]=tolower(corrente.ingredienti_piatto[0][i]);
        for(i=0; i<strlen(corrente.ingredienti_piatto[1]); i++)
            corrente.ingredienti_piatto[1][i]=tolower(corrente.ingredienti_piatto[1][i]);
        for(i=0; i<strlen(corrente.ingredienti_piatto[2]); i++)
            corrente.ingredienti_piatto[2][i]=tolower(corrente.ingredienti_piatto[2][i]);


    printf("id=%d\n", corrente.id_piatto);
    printf("nome= %s\n", corrente.nome_piatto);
    printf("tipologia= %d\n", corrente.tipologia_piatto);
    printf("Ingredienti= %s, %s, %s\n", corrente.ingredienti_piatto[0], corrente.ingredienti_piatto[1], corrente.ingredienti_piatto[2]);
    printf("prezzo=%f\n", corrente.prezzo_piatto);

    /**Tutte le informazioni relative al piatto sono state controllate e "ripulite", ora posso inserire il piatto nel vettore*/

    pacchetto->vettore=(Piatto*)calloc(1, sizeof(Piatto));          ///A questo punto alloco 1*sizeof(Piatto) bytes di memoria e li metto a disposizione del vettore di piatti
    pacchetto->vettore[0]=corrente;                                 ///Copio il contenuto della variabile d'appoggio nella prima cella del vettore appena allocato
    pacchetto->dim_vettore=1;                                       ///Tengo conto di quanti piatti ho memorizzato nel menu, quindi in questo caso solo 1

    fp=fopen("piatti.dat", "wb");                                   ///Apro il file binario dei piatti in modalit� di sola scrittura perch� devo memorizzare il primo piatto
    dim = fwrite (pacchetto->vettore, sizeof (Piatto), 1, fp);      ///Salvo nella variabile dim l'esito della scrittura su file del piatto
    printf("dim=%d, sizeof(Piatto)=%d\n", dim, sizeof(Piatto));
    if (dim*sizeof(Piatto) == (int)sizeof (Piatto))                 ///E controllo se la scrittura � avvenuta con successo
        printf ("Scrittura OK\n");
    fclose(fp);                                                     ///Chiudo il file
    inserisci_scorte(corrente);                                     ///Invoco la procedura inserisci_scorte che mi permette di caricare gli ingredienti usati per preparare il piatto inserito

    /** I quattro comandi successivi servono a creare una finestra di tipo dialog e collegare all'evento click del pulsante ok la chiusura della finestra di aggiunta piatto **/
    pacchetto->dialog=gtk_message_dialog_new (GTK_WINDOW (pacchetto->finestra_aggiungi_piatto),GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, "Nuovo piatto aggiunto!");
    gtk_dialog_run(GTK_DIALOG(pacchetto->dialog));
    gtk_widget_destroy (pacchetto->dialog);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_ok), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto->finestra_aggiungi_piatto);

}

void inserisci_piatto(GtkWidget *pulsante, Elementigtk *pacchetto)                              ///Con quest'altra invece vengono inseriti i piatti successivo al primo
{
    Piatto buffer;                          ///Dichiaro una variabile di tipo piatto tramite la quale verificher� se il piatto inserito � gi� presente nel menu
    Piatto *vet_temp;                       ///Creo un puntatore ad una struttura di tipo Piatto, mi servir� come vettore di appoggio per evitare di usare la realloc
    int i, j, dim=pacchetto->dim_vettore;   ///Dichiaro due contatori e una variabile nella quale memorizzo la dimensione corrente del vettore
    size_t len;                             ///Variabile che mi serve per verificare se l'allocazione � andata a buon fine
    char str_appoggio[100];                 ///Stringa di appoggio che mi serve per far si che l'istruzione gtk_entry_get_text vada sempre a buon fine
    FILE *fp=NULL;                          ///Dichiaro un puntatore a file e inizializzo una finestra di dialogo
    pacchetto->dialog=gtk_message_dialog_new (GTK_WINDOW (pacchetto->finestra_aggiungi_piatto),GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "Errore: nome gia' presente!");


    buffer=controlla_nome(pacchetto->nome, pacchetto);      ///Controllo se il nome inserito � gi� presente nel menu tramite apposita funzione
    if(buffer.id_piatto!=-1)                                ///Se � gi� presente..
    {
        printf("\nErrore: nome gia' presente!\n");
        gtk_dialog_run(GTK_DIALOG(pacchetto->dialog));      ///...avviso l'utente con la finestra di dialogo precedentemente creata,
        gtk_widget_destroy(pacchetto->dialog);              ///distruggo la finestra di dialogo
        return;                                             ///ed esco
    }

    printf("\nOk, il nome pu� andar bene! RIALLOCO\n");     ///EVITO DI USARE LA REALLOC

    vet_temp=(Piatto*)calloc((dim+1), sizeof(Piatto));      ///Alloco il vettore temporaneo con la dimensione del vettore di piatti pi� uno, perch� devo aggiungerne uno
    for(i=0; i<dim; i++)                                    ///Faccio scorrere il vettore temporaneo
    {
        vet_temp[i]=pacchetto->vettore[i];                  ///e memorizzo all'interno tutti i piatti contenuti nel vettore di piatti
    }
    vet_temp[dim].id_piatto=dim+1;                          ///Tengo conto del nuovo piatto che sto aggiungendo

    strcpy(vet_temp[dim].nome_piatto ,gtk_entry_get_text(GTK_ENTRY(pacchetto->nome)));  ///Copio nell'ultima posizione del vettore temporaneo tutte le informazioni presenti nelle entry della finestra: nome,
    vet_temp[dim].tipologia_piatto=(Tipologie)pacchetto->tip_enum;                      ///..tipologia,
    strcpy(str_appoggio, gtk_entry_get_text(GTK_ENTRY(pacchetto->ingrediente1)));       ///..ingredienti
    strcpy(vet_temp[dim].ingredienti_piatto[0] ,str_appoggio);
    strcpy(str_appoggio, gtk_entry_get_text(GTK_ENTRY(pacchetto->ingrediente2)));
    strcpy(vet_temp[dim].ingredienti_piatto[1] ,str_appoggio);
    strcpy(str_appoggio, gtk_entry_get_text(GTK_ENTRY(pacchetto->ingrediente3)));
    strcpy(vet_temp[dim].ingredienti_piatto[2] ,str_appoggio);
    strcpy(str_appoggio, gtk_entry_get_text(GTK_ENTRY(pacchetto->prezzo)));             ///e prezzo
    printf("IL NUMERO DECIMALE E' %s\n", str_appoggio);
    sscanf(str_appoggio, "%f", &vet_temp[dim].prezzo_piatto);                           ///leggo un numero dalla stringa e lo assegno al campo prezzo del piatto corrente

    for(i=0; i<dim+1; i++)                                                              ///Faccio in modo che tutte le stringhe degli ingredienti siano scritte in minuscolo, per comodit� e per una questione estetica
    {
        for(j=0; j<strlen(vet_temp[i].ingredienti_piatto[0]); j++)
            vet_temp[i].ingredienti_piatto[0][j]=tolower(vet_temp[i].ingredienti_piatto[0][j]);
        for(j=0; j<strlen(vet_temp[i].ingredienti_piatto[1]); j++)
            vet_temp[i].ingredienti_piatto[1][j]=tolower(vet_temp[i].ingredienti_piatto[1][j]);
        for(j=0; j<strlen(vet_temp[i].ingredienti_piatto[2]); j++)
            vet_temp[i].ingredienti_piatto[2][j]=tolower(vet_temp[i].ingredienti_piatto[2][j]);
    }

    printf("OKNOME id=%d\n", vet_temp[dim].id_piatto);
    printf("nome= %s\n", vet_temp[dim].nome_piatto);
    printf("tipologia= %d\n", vet_temp[dim].tipologia_piatto);
    printf("Ingredienti= %s, %s, %s\n", vet_temp[dim].ingredienti_piatto[0], vet_temp[dim].ingredienti_piatto[1], vet_temp[dim].ingredienti_piatto[2]);
    printf("prezzo=%f\n", vet_temp[dim].prezzo_piatto);

    pacchetto->vettore=calloc((dim+1), sizeof(Piatto));                     ///Infine rialloco e azzero il vettore dei piatti con la dimensione giusta
    for(i=0; i<dim+1; i++)                                                  ///e con un ciclo for faccio scorrere tutte le celle del vettore
    {
        pacchetto->vettore[i]=vet_temp[i];                                  ///per copiare all'interno l'intero contenuto del vettore temporaneo, in questo modo evito le perdite
    }                                                                       ///di informazioni dovute all'utilizzo della realloc


    fp=fopen ("piatti.dat", "a+b");                                         ///Apro il file dei piatti in modalit� append, il cursore viene quindi spostato alla fine del file
    if(fp==NULL)                                                            ///Se il file non viene aperto correttamente
    {
        printf("Errore nell'apertura del file piatti.dat\n");               ///Avviso l'utente ed esco
        exit(-1);
    }
    else                                                                    ///Altrimenti..
    {
        len = fwrite (&pacchetto->vettore[dim], sizeof (Piatto), 1, fp);    ///...scrivo il piatto corrente sul file
        printf("len=%d, sizeof(Piatto)=%d\n", len, sizeof(Piatto));
        if (len*sizeof(Piatto) == (int)sizeof (Piatto))                     ///e tramite la variabile len verifico l'esito della scrittura
            printf ("Scrittura OK\n");
    }
    fclose(fp);                                                             ///Chiudo il file
    inserisci_scorte(pacchetto->vettore[dim]);                              ///Invoco la procedura per inserire nel file delle scorte gli ingredienti del piatto inserito

    /** I quattro comandi successivi servono a creare una finestra di tipo dialog e collegare all'evento click del pulsante ok la chiusura della finestra di aggiunta piatto **/
    pacchetto->dialog=gtk_message_dialog_new (GTK_WINDOW (pacchetto->finestra_aggiungi_piatto),GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, "Nuovo piatto aggiunto!");
    gtk_dialog_run(GTK_DIALOG(pacchetto->dialog));
    gtk_widget_destroy (pacchetto->dialog);
    g_signal_connect(G_OBJECT(pacchetto->pulsante_ok), "clicked", G_CALLBACK(distruggi), (gpointer)pacchetto);

    pacchetto->dim_vettore+=1;      ///Tengo conto del nuovo piatto inserito
    free(vet_temp);                 ///Libero la memoria allocata per il vettore temporaneo

}

void modifica_nome(GtkWidget *pulsante, Elementigtk *pacchetto)                                 ///Procedura che modifica il nome di un piatto scelto dall'utente
{
    Piatto risultato, nuovo;    ///Dichiaro due variabili di tipo Piatto, una per il nome vecchio e una per il nome nuovo
    FILE *fp=NULL;              ///e  un puntatore a file

    risultato=controlla_nome(pacchetto->entry, pacchetto);                                      ///Verifico se il nome inserito esiste effettivamente
    if(controlla_se_modificabile(pacchetto, risultato, pacchetto->finestra_modifica_piatto)==0) ///Verifico se il nome pu� essere modificato al momento
        return;
    nuovo=controlla_nome(pacchetto->e_nome, pacchetto);                                         ///Verifico se il nuovo nome inserito esiste effettivamente

    /**A seconda dei risultati ottenuti creo diverse finestre di dialogo tramite la funzione "avvisa"*/
    if(nuovo.id_piatto!=-1)         ///se il nuovo nome � gi� presente nel menu avviso ed esco
    {
        avvisa(3);
        return;
    }
    if(risultato.id_piatto==-1)     ///se il nome da modificare non compare nel menu avviso
    {
        avvisa(4);
    }
    else                            ///Altrimenti tramite strcpy sostituisco il vecchio nome col nuovo
    {
        strcpy(pacchetto->vettore[risultato.id_piatto-1].nome_piatto, gtk_entry_get_text(GTK_ENTRY(pacchetto->e_nome)));
        fp=fopen("piatti.dat", "r+b");                                  ///Apro il file binario dei piatti in modalit� ibrida lettura+scrittura
        if(fp==NULL)                                                    ///Controllo che il file sia stato aperto correttamente
        {
            printf("Errore nell'apertura del file piatti.dat\n");
            exit(-1);
        }

        printf("Locazione dell'id trovato=%d\n", risultato.id_piatto);  ///Verifico la locazione del piatto all'interno del vettore

        /**Mi sposto all'interno del vettore nella posizione del piatto da modificare**/
        fseek(fp, (pacchetto->vettore[risultato.id_piatto-1].id_piatto - 1)*(sizeof(Piatto)), SEEK_SET);
        /**A questo punto sovrascrivo il piatto modificato, controllo che la scrittura sia andata a buon termine e avviso*/
        if(fwrite(&pacchetto->vettore[risultato.id_piatto-1], sizeof(Piatto), 1, fp)==1)
        {
            printf("Scrittura avvenuta con successo, nuovo nome= %s\n", risultato.nome_piatto);
            avvisa(5);
        }
    }
    fclose(fp);     ///Chiudo il file
}

void modifica_tipologia(GtkWidget *pulsante, Elementigtk *pacchetto)                            ///Procedura che modifica la tipologia di un piatto scelto dall'utente (prenotato grazie alla funzione prenota_pasto)
{
    Piatto risultato;   ///Dichiaro una variabile di tipo piatto che mi serve per controllare la presenza del nuovo nome all'interno del vettore
    FILE *fp=NULL;      ///Dichiaro un puntatore a file

    printf("La entry del nome in tipologia vale: %s\n", gtk_entry_get_text(GTK_ENTRY(pacchetto->entry)));

    risultato=controlla_nome(pacchetto->entry, pacchetto);                                      ///Invoco la funzione controlla_nome
    if(controlla_se_modificabile(pacchetto, risultato, pacchetto->finestra_modifica_piatto)==0) ///E la funzione controlla_se_modificabile (vedi funzione precedente per ulteriori spiegazioni)
        return;
    printf("Tipologia, risultato.id_piatto=%d\n", risultato.id_piatto);
    if(risultato.id_piatto==-1)                                                                 ///Se il nome del piatto da modificare non compare nel menu avviso
    {
        printf("Tipologia: Il nome cercato non compare nel menu\n");
        avvisa(4);
    }
    else                                                                                        ///Altrimenti..
    {
        pacchetto->vettore[risultato.id_piatto-1].tipologia_piatto=pacchetto->tip_enum;         ///Sovrascrivo la tipologia del piatto esaminato con la nuova tipologia "prenotata"

        fp=fopen("piatti.dat", "r+b");                                                          ///Apro il file dei piatti in modalit� ibrida lettura + scrittura
        if(fp==NULL)                                                                            ///Controllo che il file sia stato aperto correttamente
        {
            printf("Errore nell'apertura del file piatti.dat\n");
            exit(-1);
        }

        printf("Locazione dell'id trovato=%d\n", pacchetto->vettore[risultato.id_piatto-1].id_piatto);

        fseek(fp, ((pacchetto->vettore[risultato.id_piatto-1].id_piatto)-1)*(sizeof(Piatto)), SEEK_SET);    ///Mi sposto nel file fino ad arrivare al piatto che mi interessa modificare
        if(fwrite(&pacchetto->vettore[risultato.id_piatto-1], sizeof(Piatto), 1, fp)==1)                    ///Sovrascrivo il piatto appena modificato e controllo che la scrittura sia andata a buon fine
        {
            printf("Scrittura avvenuta con successo, nuovo tipo= %d\n", pacchetto->vettore[risultato.id_piatto-1].tipologia_piatto);
            avvisa(5);                                                                                      ///Preparo una finestra di dialogo
        }
    }
    fclose(fp);     ///Chiudo il file
}

void modifica_ingrediente(GtkWidget *pulsante, Elementigtk *pacchetto)                          ///Procedura che modifica uno dei tre ingredienti di un piatto scelto dall'utente
{
    Piatto risultato;                                               ///Dichiaro una variabile di tipo Piatto
    FILE *fp=NULL;                                                  ///Dichiaro un puntatore a file
    char stringa[20];                                               ///Dichiaro una stringa che mi permette di sapere tramite pulsante quale ingrediente bisogna modificare
    int n_ingrediente, i;                                              ///Variabile nella quale viene memorizzato il numero dell'ingrediente che deve essere modificato
    const gchar *testo=gtk_button_get_label((GtkButton*)pulsante);  ///Dichiaro una variabile di tip gchar sulla quale memorizzo la stringa presente sul pulsante premuto dall'utente
    strcpy(stringa, testo);                                         ///Copio il contenuto della variabile gchar in una stringa
    sscanf(stringa, "Ingrediente %d", &n_ingrediente);              ///Leggo la stringa fino a trovare un numero, che memorizzo nella variabile n_ingrediente
    n_ingrediente--;                                                ///Decremento di 1 la variabile. Ora n_ingrediente indica la cella del vettore ingredienti_piatto che deve essere modificata

    risultato=controlla_nome(pacchetto->entry, pacchetto);          ///Controllo che il nome del piatto da modificare sia presente nel menu
    if(controlla_se_modificabile(pacchetto, risultato, pacchetto->finestra_modifica_piatto)==0) ///...e che sia modificabile
        return;                                                     ///se non � modificabile esco

    if(risultato.id_piatto==-1 )                                    ///Se il nome cercato non compare nel menu creo una finestra di dialogo per avvisare l'utente
    {
        printf("Il nome cercato non compare nel menu\n");
        avvisa(4);
    }
    else                                                            ///Altrimenti, a seconda dell'ingrediente da cambiare...
    {
        switch(n_ingrediente)                                       ///..sovrascrivo l'ingrediente da modificare con quello inserito nella entry corrispondente, eliminando le lettere maiuscole
        {
        case 0:
            strcpy(pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente], gtk_entry_get_text(GTK_ENTRY(pacchetto->e_ing1)));
            /**Controllo per eliminare le lettere maiuscole, uguale in tutti i casi*/
            for(i=0; i<strlen(pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente]); i++)
                pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente][i]=tolower(pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente][i]);
            break;
        case 1:
            strcpy(pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente], gtk_entry_get_text(GTK_ENTRY(pacchetto->e_ing2)));
            for(i=0; i<strlen(pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente]); i++)
                pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente][i]=tolower(pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente][i]);
            break;
        case 2:
            strcpy(pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente], gtk_entry_get_text(GTK_ENTRY(pacchetto->e_ing3)));
            for(i=0; i<strlen(pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente]); i++)
                pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente][i]=tolower(pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente][i]);
            break;
        default:
            printf("Errore nello switch-case\n");
            break;
        }

        fp=fopen("piatti.dat", "r+b");                              ///Apro il file dei piatti in modalit� ibrida lettura+scrittura
        if(fp==NULL)                                                ///Verifico che il file sia stato aperto correttamente
        {
            printf("Errore nell'apertura del file piatti.dat\n");
            exit(-1);
        }
        printf("Locazione dell'id trovato=%d\n", pacchetto->vettore[risultato.id_piatto-1].id_piatto);
        /**Mi sposto nella posizione desiderata all'interno del file, sovrascrivo il piatto del quale ho modificato un ingrediente e verifico che sia andato tutto bene*/
        fseek(fp, (pacchetto->vettore[risultato.id_piatto-1].id_piatto - 1)*(sizeof(Piatto)), SEEK_SET);
        if(fwrite(&pacchetto->vettore[risultato.id_piatto-1], sizeof(Piatto), 1, fp)==1)
        {
            printf("Scrittura avvenuta con successo, nuovo ingrediente= %s\n", pacchetto->vettore[risultato.id_piatto-1].ingredienti_piatto[n_ingrediente]);
            avvisa(5);
        }
    }
    inserisci_scorte(pacchetto->vettore[risultato.id_piatto-1]);    ///Invoco la funzione che mi permette di inserire l'ingrediente appena modificato all'interno del file delle scorte
    fclose(fp);     ///Chiudo il file
}

void modifica_prezzo(GtkWidget *pulsante, Elementigtk *pacchetto)                               ///Procedura che modifica il prezzo di un piatto scelto dall'utente
{
    Piatto risultato;                                           ///Dichiaro una variabile di tipo piatto,
    char stringa[NOME];                                         ///una stringa che mi serve per trasformare la entry del prezzo in un numero
    FILE *fp=NULL;                                              ///e un puntatore a file
    risultato=controlla_nome(pacchetto->entry, pacchetto);      ///Controllo che il nome inserito sia presente nel menu
    if(controlla_se_modificabile(pacchetto, risultato, pacchetto->finestra_modifica_piatto)==0) ///e che non sia modificabile
        return;

    if(risultato.id_piatto==-1)                                 ///Se il nome cercato non compare nel menu, avviso l'utente
    {
        printf("Prezzo: Il nome cercato non compare nel menu\n");
        avvisa(4);
    }
    else                                                        ///Altrimenti...
    {
        strcpy(stringa, gtk_entry_get_text(GTK_ENTRY(pacchetto->e_prezzo)));                ///copio tutto quello che c'� scritto nella entry del prezzo all'interno della stringa
        sscanf(stringa, "%f", &pacchetto->vettore[risultato.id_piatto-1].prezzo_piatto);    ///e cerco all'interno della stringa un numero

        fp=fopen("piatti.dat", "r+b");                                                      ///Apro il file dei piatti in modalit� ibrida e verifico che sia realmente aperto
        if(fp==NULL)
        {
            printf("Errore nell'apertura del file piatti.dat\n");
            exit(-1);
        }
        printf("Locazione dell'id trovato=%d\n", pacchetto->vettore[risultato.id_piatto-1].id_piatto);
        /**Mi sposto nella posizione desiderata all'interno del file, sovrascrivo il piatto del quale ho modificato il prezzo e verifico che sia andato tutto bene*/
        fseek(fp, (pacchetto->vettore[risultato.id_piatto-1].id_piatto - 1)*(sizeof(Piatto)), SEEK_SET);
        if(fwrite(&pacchetto->vettore[risultato.id_piatto-1], sizeof(Piatto), 1, fp)==1)
        {
            printf("Scrittura avvenuta con successo, nuovo prezzo= %.2f\n", pacchetto->vettore[risultato.id_piatto-1].prezzo_piatto);
            avvisa(5);
        }
        gtk_widget_show_all(pacchetto->finestra_modifica_piatto);
    }
    fclose(fp);
}

void elimina_un_piatto(GtkWidget *pulsante, Elementigtk *pacchetto)                             ///Questa procedura si occupa di eliminare dal menu un piatto scelto dall'utente
{
    FILE *fp=NULL;                                          ///Dichiaro un puntatore a file,
    Piatto risultato, *vet_temp;                            ///una variabile e un puntatore di tipo Piatto,
    int i=0, flag=0, dim=pacchetto->dim_vettore;            ///un contatore, una variabile con funzione di flag e una variabile dim che inizializzo con la dimensione corrente del vettore

    risultato=controlla_nome(pacchetto->entry, pacchetto);  ///Controllo che il nome del piatto da modificare esista nel menu
    if(controlla_se_modificabile(pacchetto, risultato, pacchetto->finestra_elimina_piatto)==0)  ///e che sia modificabile
        return;                                             ///se non � modificabile esco
    if(risultato.id_piatto==-1)                             ///Se il nome non � presente nel menu avviso l'utente
    {
        printf("Il nome cercato non compare nel menu\n");
        avvisa(4);
    }
    else                                                    ///Altrimenti..
    {
        vet_temp=(Piatto*)calloc(dim-1, sizeof(Piatto));    ///alloco il vettore temporaneo con la dimensione del vettore dei piatti decrementata di uno
        for(i=0; i<dim; i++)                                                            ///Faccio scorrere tutto il vettore dei piatti
        {
            if(pacchetto->vettore[i].id_piatto!=risultato.id_piatto && flag==0)         ///Fin quando non trovo il piatto da eliminare..
            {
                vet_temp[pacchetto->vettore[i].id_piatto-1]=pacchetto->vettore[i];      ///..semplicemente copio i piatti nel vettore temporaneo
            }
            else if(pacchetto->vettore[i].id_piatto!=risultato.id_piatto && flag==1)    ///Se invece ho gi� trovato il piatto da eliminare..
            {
                pacchetto->vettore[i].id_piatto-=1;                                     ///..decremento di 1 l'id del piatto per tenere i piatti in ordine
                vet_temp[pacchetto->vettore[i].id_piatto-1]=pacchetto->vettore[i];      ///e copio il piatto nel vettore temporaneo
            }
            else                                                                        ///Se il piatto da eliminare � quello esaminato..
            {
                flag=1;                                                                 ///..non lo copio � inizializzo la flag a 1
            }
        }
        pacchetto->vettore=(Piatto*)calloc(dim-1, sizeof(Piatto));                      ///A questo punto alloco il vettore di piatti con la dimensione giusta e lo azzero
        fp=fopen("piatti.dat", "wb");                                                   ///Dato che ci sono apro pure il file dei piatti e lo azzero (modalit� scrittura)
        for(i=0; i<dim-1; i++)                                                          ///Faccio ciclare il vettore di piatti e copio individualmente ogni piatto presente in vet_temp...
        {
            pacchetto->vettore[i]=vet_temp[i];                                          ///..sia nel vettore di piatti
            fwrite(&pacchetto->vettore[i], sizeof(Piatto), 1, fp);                      ///che nel file
        }
        fclose(fp);                                                                     ///Chiudo il file
        pacchetto->dim_vettore-=1;                                                      ///Tengo conto del piatto eliminato
        printf("Eliminazione avvenuta con successo!\n");
        avvisa(6);                                                                      ///e avviso l'utente che tutto � andato bene
    }
    free(vet_temp);                                                                     ///Libero la memoria allocata per il vettore temporaneo
}

void elimina_tutto(GtkWidget *pulsante, Elementigtk *pacchetto)                                 ///Con questa procedura � possibile eliminare tutti i piatti presenti sia sul file che sul vettore di piatti
{
    remove("piatti.dat");                                   ///Elimino il file dei piatti
    pacchetto->vettore=(Piatto*)calloc(0, sizeof(Piatto));  ///Alloco 0 byte e azzero il vettore dei piatti
    free(pacchetto->vettore);                               ///Libero la memoria allocata
    pacchetto->dim_vettore=0;                               ///Azzero il contatore di piatti presenti nel menu
    avvisa(6);                                              ///Avviso l'utente
    gtk_widget_destroy(pacchetto->finestra_elimina_piatto); ///Chiudo la finestra di eliminazione, tanto ormai non serve pi�..
}

void ricerca_per_ingrediente(GtkWidget(*pulsante_modifica), Elementigtk *pacchetto)             ///Questa procedura effettua una ricerca di tutti i piatti che contengono un certo ingrediente scelto dall'utente
{
    /**Creo la finestra nella quale verranno visualizzati i piatti trovati e la personalizzo*/
    GtkWidget *finestra=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (finestra), "Risultati ricerca");
    gtk_window_set_default_size(GTK_WINDOW(finestra), 500, 800);
    gtk_window_set_position (GTK_WINDOW (finestra), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (finestra) ,15);
    /**Creo una vbox e la inserisco nella finestra, poi inserisco una label dentro la vbox*/
    GtkWidget *vbox_main=gtk_vbox_new(FALSE, 6);
    gtk_container_add (GTK_CONTAINER (finestra), vbox_main);
    GtkWidget *label=gtk_label_new("\nI seguenti piatti contengono\n  l'ingrediente da lei cercato");
    gtk_box_pack_start (GTK_BOX (vbox_main), label, FALSE, FALSE, 0);
    /**Creo una finestra scrolled e la inserisco dentro la prima vbox, in seguito inserisco una seconda vbox dentro la finestra scrolled*/
    pacchetto->finestra_scrolled=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(vbox_main), pacchetto->finestra_scrolled);
    GtkWidget *vbox=gtk_vbox_new(FALSE, 6);
    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (pacchetto->finestra_scrolled), vbox);
    /**Creo un pulsante che mi permette di tornare alla finestra precedente*/
    GtkWidget *pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox_main), pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)finestra);

    char stringa[NOME];                                                                     ///e una stringa per memorizzare una entry
    int i, j, k, dim=pacchetto->dim_vettore, flag=0;                                        ///Dichiaro un contatore e una variabile per memorizzare la dimensione corrente del vettore
    strcpy(stringa, gtk_entry_get_text((GtkEntry*)pacchetto->entry));                       ///Copio il contenuto della entry nella stringa precedentemente creata

    for(i=0; i<dim; i++)                                                                    ///Faccio scorrere tutto il vettore di piatti alla ricerca del piatto
    {
        for(j=0; j<N_ING; j++)                                                              ///Faccio scorrere tutti gli ingredienti di ciascun piatto..
        {
            for(k=0; k<strlen(pacchetto->vettore[i].ingredienti_piatto[j]); k++)            ///e tutte le lettere contenute nel nome dell'ingrediente corrente
            {
                printf("%c - %c\n", tolower(pacchetto->vettore[i].ingredienti_piatto[j][k]), tolower(stringa[k]));
                if(tolower(pacchetto->vettore[i].ingredienti_piatto[j][k]) != tolower(stringa[k]) ||    ///Se la lettera corrente dell'ingrediente non corrisponde alla lettera corrente dell'ingrediente inserito
                   strlen(pacchetto->vettore[i].ingredienti_piatto[j]) != strlen(stringa))              ///o se i due ingredienti presentano un numero di lettere diverso..
                {
                    flag=0;                                                                             ///Imposto la flag a 0 ed esco
                    break;
                }
                else                                                                                    ///Altrimenti
                {
                    flag++;                                                                             ///incremento la flag
                    printf("flag=%d, strlen(%s)=%d\n", flag, pacchetto->vettore[i].ingredienti_piatto[j], strlen(pacchetto->vettore[i].ingredienti_piatto[j]));
                    if(flag==strlen(pacchetto->vettore[i].ingredienti_piatto[j]))                       ///Se l'ingrediente inserito � presente nel vettore di piatti
                    {
                        char stringa[NOME]="\0";                                                        ///..creo una stringa provvisoria da passare alla funzione stampa_pietanza..
                        stampa_pietanza(pacchetto->vettore[i], label, stringa, vbox);                   ///Infine invoco la funzione per stampare la pietanza all'interno della vbox sotto forma di label
                    }
                }
            }
        }
    }
    gtk_widget_show_all(finestra);                                                          ///Rendo visibile la finestra

}

void prendi_ordinazione(GtkWidget *pulsante, Scontrino scontrini[N_TAVOLI])                     ///Questa procedura si occupa di salvare un'ordinazione di un tavolo nel vettore delle ordinazioni di quel tavolo
{
    printf("Ok prendi ordinazione\n");
    char stringa[1000];                                                     ///Dichiaro una stringa che mi servir� per prendere in ingresso da una label di un pulsante diverse informazioni
    Piatto buffer;                                                          ///Dichiaro una variabile d'appoggio di tipo Piatto
    int numero_tavolo, tip;                                                 ///e due variabili intere che mi servono per memorizzare il numero del tavolo servito e la tipologia del piatto (la seconda mi serve per evitare un warning in particolare)
    strcpy(stringa, (char*)(gtk_button_get_label((GtkButton*)pulsante)));   ///Copio nella stringa tutto il testo contenuto nella label contenuta nel pulsante premuto dall'utente, corrispondente ad un piatto

    /**A questo punto eseguo una ricerca all'interno della stringa mirata a ricercare tutte le informazioni sul tavolo servito e sul piatto corrente.
       Tutto questo � necessario perch� una funzione GTK come questa accetta in ingresso solo un parametro, e io ho la necessit� di passare alla funzione
       prima di tutto la struttura di scontrini.
       Salvo quindi tutte le informazioni del piatto nella variabile buffer (tranne la tipologia, che memorizzo prima in tip per evitare un warning) **/

    sscanf(stringa,  "[Tavolo %d] Piatto #%d:\n\n%[^\n]\n(Tipologia %d)\nIngredienti: %[^,], %[^,], %[^\n]\nPrezzo: %f", &numero_tavolo, &buffer.id_piatto, buffer.nome_piatto, &tip, buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], &buffer.prezzo_piatto);

    printf("\n\nTAVOLO %d, id=%d, nome=%s\n", numero_tavolo, buffer.id_piatto, buffer.nome_piatto);
    buffer.tipologia_piatto=tip;                                                                ///copio il valore di tip nel campo tipologia_piatto della variabile buffer
    printf("La tipologia e' %d\n", buffer.tipologia_piatto);
    printf("Ingredienti=%s, %s, %s\nPrezzo=%.2f\n\nOK IL PIATTO E' PRONTO!\n", buffer.ingredienti_piatto[0], buffer.ingredienti_piatto[1], buffer.ingredienti_piatto[2], buffer.prezzo_piatto);
    //ORA HO IL PIATTO PRONTO DA SERVIRE
    /*for(i=0; i<N_TAVOLI; i++)
        printf("scontrini[%d].id_tavolo=%d\n", i, scontrini[i].id_tavolo);*/
    if(scontrini[numero_tavolo-1].id_tavolo!=numero_tavolo)                                     ///Se il tavolo non ha ancora ordinato (cio� se l'id del tavolo non corrisponde al suo vero id bens� a -1)
    {
        printf("\n-----------------\nIL TAVOLO NON HA ANCORA ORDINATO\n");
        printf("\nUNO= %d, DUE=%d\n", scontrini[numero_tavolo-1].id_tavolo, numero_tavolo);
        scontrini[numero_tavolo-1].ordini=(Ordine*)calloc(1, sizeof(Ordine));                   ///Alloco 1*sizeof(Ordini) bytes e li metto a disposizione del campo ordini della struttura scontrini (per il tavolo corrente)

        scontrini[numero_tavolo-1].id_tavolo=numero_tavolo;                                     ///Infine salvo nella struttura scontrini l'id del tavolo servito (che in precedenza avevo inizializzato a -1),
        scontrini[numero_tavolo-1].ordini[0].corrente=buffer;                                   ///..l'ordinazione corrente..
        scontrini[numero_tavolo-1].ordini[0].quantita=1;                                        ///..imposto come quantit� 1..
        scontrini[numero_tavolo-1].num_ordinazioni=1;                                           ///e tengo conto di quante ordinazioni ha chiesto il tavolo (questa � la prima)

        printf("***PRIMA ORDINAZIONE: [Tavolo %d]\nPiatto #%d: %s, (Tipologia %d ), quantita=%d\n%s,%s,%s\n%.2f", scontrini[numero_tavolo-1].id_tavolo,
               scontrini[numero_tavolo-1].ordini[0].corrente.id_piatto,
               scontrini[numero_tavolo-1].ordini[0].corrente.nome_piatto,
               scontrini[numero_tavolo-1].ordini[0].corrente.tipologia_piatto,
               scontrini[numero_tavolo-1].ordini[0].quantita,
               scontrini[numero_tavolo-1].ordini[0].corrente.ingredienti_piatto[0],
               scontrini[numero_tavolo-1].ordini[0].corrente.ingredienti_piatto[1],
               scontrini[numero_tavolo-1].ordini[0].corrente.ingredienti_piatto[2],
               scontrini[numero_tavolo-1].ordini[0].corrente.prezzo_piatto);

        avvisa(0);                                                                              ///Avviso l'utente che � andato tutto bene
        return;                                                                                 ///Esco
    }
    else                                                                                                ///Altrimenti, se non � la prima ordinazione..
    {
        int num_ordinazioni=scontrini[numero_tavolo-1].num_ordinazioni, i;                              ///Dichiaro una variabile per tener conto di quante ordinazioni il tavolo ha gi� richiesto ed un contatore
        static int gia_ordinato=0;                                                                      ///Dichiaro una variabile per sapere se un tavolo ha gi� ordinato oppure no
        printf("\n------------\nIL TAVOLO HA GIA' ORDINATO, num_ordinazioni=%d\n", num_ordinazioni);

        for(i=0; i<num_ordinazioni; i++)                                                                ///Controllo se il piatto ordinato � gi� stato ordinato in precedenza dal tavolo corrente
        {
            printf("\nID_ESAMINANDO: %d, ID_ESAMINATO: %d\n", buffer.id_piatto, scontrini[numero_tavolo-1].ordini[i].corrente.id_piatto);
            if(buffer.id_piatto==scontrini[numero_tavolo-1].ordini[i].corrente.id_piatto)               ///Se il piatto � presente nel vettore di ordini del tavolo corrente (quindi se � gia stato ordinato)
            {
                scontrini[numero_tavolo-1].ordini[i].quantita+=1;                                       ///Aumento semplicemente la quantit� del piatto ordinato di uno
                gia_ordinato=1;                                                                         ///e inizializzo la flag gia_ordinato a 1
                avvisa(0);                                                                              ///Avviso l'utente
                return;                                                                                 ///ed esco
            }
        }

        if(gia_ordinato==0);                                                                ///Se il piatto selezionato non � gi� stato ordinato dal tavolo corrente..
        {
            printf("\n\n---------------RIALLOCO------------------\n\n");
            Scontrino temp;                                                                 ///Dichiaro una variabile temporanea di tipo scontrino
            temp.ordini=(Ordine*) calloc (num_ordinazioni+1, sizeof(Ordine));               ///Alloco tanto spazio quanto basta ber contenere un'ordinazione in pi� e lo metto a disposizione del vettore temporaneo
            temp.id_tavolo=scontrini[numero_tavolo-1].id_tavolo;                            ///Copio tutto il contenuto del vettore di scontrini nel vettore temporaneo: l'id del tavolo
            temp.prezzo_totale=scontrini[numero_tavolo-1].prezzo_totale;                    ///..il totale del prezzo,
            temp.num_ordinazioni=scontrini[numero_tavolo-1].num_ordinazioni;                ///..il numero di piatti ordinati dal tavolo (due ordinazioni dello stesso piatto contano come una)
            for(i=0; i<num_ordinazioni; i++)                                                ///..e tutti i piatti ordinati dal tavolo (compressa la quantit�)
            {
                temp.ordini[i].corrente=scontrini[numero_tavolo-1].ordini[i].corrente;
                temp.ordini[i].quantita=scontrini[numero_tavolo-1].ordini[i].quantita;
            }

            temp.ordini[num_ordinazioni].corrente=buffer;                                   ///Salvo l'ultimo piatto ordinato nel vettore di ordini del tavolo corrente
            temp.ordini[num_ordinazioni].quantita=1;                                        ///con quantit� pari a 1

            scontrini[numero_tavolo-1].ordini=(Ordine*)calloc(1, sizeof(temp.ordini));      ///In questo modo evito la realloc, posso ora allocare con uno spazio maggiore ed azzerare il vecchio vettore

            scontrini[numero_tavolo-1]=temp;                                                ///e copiare il contenuto del vettore temporaneo nel vettore appena allocato

            scontrini[numero_tavolo-1].num_ordinazioni+=1;                                  ///Tengo conto dell'ordinazione appena effettuata
            avvisa(0);                                                                      ///Avviso l'utente

            temp.ordini=(Ordine*) calloc (0, sizeof(Ordine));
            free(temp.ordini);                                                              ///Libero la memoria allocata in precedenza per il vettore temporaneo
        }
        gia_ordinato=0;                                                                     ///Azzero la variabile gia_ordinato per sicurezza
        for(i=0; i<num_ordinazioni; i++)
        {
            printf("\n------------\nORDINAZIONE: [Tavolo %d]\nPiatto #%d: %s, (Tipologia %d ), quantita=%d\n%s,%s,%s\n%.2f\n-------------\n",
                   scontrini[numero_tavolo-1].id_tavolo,
                   scontrini[numero_tavolo-1].ordini[i].corrente.id_piatto,
                   scontrini[numero_tavolo-1].ordini[i].corrente.nome_piatto,
                   scontrini[numero_tavolo-1].ordini[i].corrente.tipologia_piatto,
                   scontrini[numero_tavolo-1].ordini[i].quantita,
                   scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[0],
                   scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[1],
                   scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[2],
                   scontrini[numero_tavolo-1].ordini[i].corrente.prezzo_piatto);
        }
    }
    printf("\nSONO USCITO\n");
}

void stampa_scontrino(GtkWidget *pulsante, Scontrino scontrini[N_TAVOLI])                       ///Con questa procedura viene salvato su file lo scntrino relativo alle ordinazioni di un tavolo
{
    printf("\nOk stampa scontrino\n");
    FILE *fp=NULL;                                                              ///Dichiaro un puntatore a file
    char str_temp[40], finale[50], str_piatto[200];                             ///e tre stringhe che user� in seguito
    const gchar *label_temp;                                                    ///variabile che mi serve per eseguire correttamente l'istruzione gtk_button_get_label
    int numero_tavolo, num_ordinazioni;                                         ///due variabili che mi servono da "segnaposto"
    int i, fA=0, fP=0, fS=0, fC=0, fD=0;                                        ///un contatore pi� 5 flag, una per ogni tipologia di piatto
    float val_sconto=0, importo_totale;                                         ///due variabili che mi serviranno per la stampa dello scontrino
    Tipologie tipo;                                                             ///Una variabile temporanea che passer� ad una funzione come argomento

    remove("scontrino.txt");                                                    ///Elimino il vecchio scontrino, tanto non mi serve pi�..

    label_temp=gtk_button_get_label((GtkButton*)pulsante);                      ///Leggo quello che c'� scritto nel pulsante
    printf("NEL PULSANTE C'E' SCRITTO %s\n", label_temp);
    strcpy(str_temp, label_temp);                                               ///e lo memorizzo in una stringa
    sscanf(str_temp, "Tavolo %d: stampa scontrino", &numero_tavolo);            ///Leggo il numero del tavolo dalla stringa e lo memorizzo in una variabile apposita
    printf("\nIL NUMERO DEL TAVOLO E' %d\n", numero_tavolo);
    num_ordinazioni=scontrini[numero_tavolo-1].num_ordinazioni;                 ///Salvo in una variabile il numero di ordinazioni del tavolo corrente (per comodit�)
    scontrini[numero_tavolo-1].prezzo_totale=0;                                 ///Azzero la variabile che tiene conto del prezzo totale per sicurezza

    fp=fopen("scontrino.txt", "a+");                                            ///Apro il file degli scontrini in modalit� ibrida append
    if(fp==NULL)                                                                ///Controllo che sia stato aperto correttamente
    {
        printf("Errore nell'apertura del file scontrino.txt\n");
        exit(-1);
    }
    for(i=0; i<num_ordinazioni; i++)                                            ///Faccio scorrere tutte le ordinazioni del tavolo corrente
    {
        switch(scontrini[numero_tavolo-1].ordini[i].corrente.tipologia_piatto)  ///Suddivido in base alla tipologia del piatto
        {
        case antipasto:                                                         ///A seconda della tipologia, inizializzo la flag corrispondente a 1
            fA=1;
            break;
        case primo:
            fP=1;
            break;
        case secondo:
            fS=1;
            break;
        case contorno:
            fC=1;
            break;
        case dessert:
            fD=1;
            break;
        default:
            printf("Errore switch-case\n");
            break;
        }
    }
    if (scontrini[numero_tavolo-1].id_tavolo==-1)                               ///Se il tavolo non ha ancora ordinato avviso l'utente, chiudo il file ed esco
    {
        avvisa(1);
        fclose(fp);
        return;
    }
    /**Inizio a stampare lo scontrino su file**/
    fprintf(fp, "--------------------------------------------------Ristorante da Nico--------------------------------------------------\n\n");
    fprintf(fp, "Tavolo: %d\n", numero_tavolo);                                 ///Stampo il numero del tavolo
    if(fA==1)                                                                   ///Stampo una tipologia di piatto solo se � stato ordinato (vale per tutti gli if successivi
    {
        fprintf(fp, "Antipasti\n");                                             ///Stampo la tipologia
        tipo=antipasto;                                                         ///Inizializzo la variabile tipo, che passer� alla funzione fai_il_conto
        fai_il_conto(scontrini, numero_tavolo, fp, tipo);                       ///Invoco la funzione che mi permette di stampare il piatto sul file
    }
    if(fP==1)                                                                   ///Idem...
    {
        fprintf(fp, "Primi piatti\n");
        tipo=primo;
        fai_il_conto(scontrini, numero_tavolo, fp, tipo);
    }
    if(fS==1)
    {
        fprintf(fp, "Secondi piatti\n");
        tipo=secondo;
        fai_il_conto(scontrini, numero_tavolo, fp, tipo);
    }                                                                           ///...
    if(fC==1)
    {
        fprintf(fp, "Contorni\n");
        tipo=contorno;
        fai_il_conto(scontrini, numero_tavolo, fp, tipo);
    }
    if(fD==1)
    {
        fprintf(fp, "Desserts\n");
        tipo=dessert;
        fai_il_conto(scontrini, numero_tavolo, fp, tipo);
    }                                                                           ///...fin qui

    /**Stampo l'importo parziale, dato dalla somma di tutti gli importi*/
    fprintf(fp, "\nImporto parziale:   \t\t\t\t\t\t\t\t\t\t\t\t%.2f�\n", scontrini[numero_tavolo-1].prezzo_totale);

    /**Controllo se l'importo parziale � >40, in tal caso applico uno sconto al prezzo totale e stampo il valore dello sconto*/
    if(scontrini[numero_tavolo-1].prezzo_totale>40)
    {
        val_sconto=(scontrini[numero_tavolo-1].prezzo_totale*SCONTO)/100;
        fprintf(fp, "Valore sconto (%d%%)   \t\t\t\t\t\t\t\t\t\t\t\t%.2f�\n", SCONTO, val_sconto);
    }

    /**Calocolo ora l'importo totale e lo stampo*/
    importo_totale=scontrini[numero_tavolo-1].prezzo_totale-val_sconto;
    fprintf(fp, "Importo totale:   \t\t\t\t\t\t\t\t\t\t\t\t%.2f�\n\n", importo_totale);
    fprintf(fp, "----------------------------------------------------------------------------------------------------------------------\n\n");

    sprintf(finale, "Il conto e' di %.2f Euro. Grazie e arrivederci.", importo_totale);     ///Scrivo l'importo totale su una stringa che verr� visualizzata su una finestra

    /**VISUALIZZO LO SCONTRINO SU SCHERMO*/
    /**Creo una finestra, la personalizzo e ci metto dentro una vbox, un'icona e una label*/
    GtkWidget *finestra=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (finestra), "Scontrino");
    gtk_window_set_default_size(GTK_WINDOW(finestra), 500, 780);
    gtk_window_set_position (GTK_WINDOW (finestra), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width (GTK_CONTAINER (finestra) ,15);
    GtkWidget *vbox_main=gtk_vbox_new(FALSE, 6);
    gtk_container_add (GTK_CONTAINER (finestra), vbox_main);
    GtkWidget *icona=gtk_image_new_from_file("Scontrino.png");
    gtk_box_pack_start (GTK_BOX (vbox_main), icona, FALSE, FALSE, 0);
    GtkWidget *label=gtk_label_new(finale);
    gtk_box_pack_start (GTK_BOX (vbox_main), label, FALSE, FALSE, 0);
    /**Creo una scrolled window e la colloco dentro la vbox principale, in seguito metto un'altra vbox dentro la finestra appena creata*/
    GtkWidget *finestra_scrolled=gtk_scrolled_window_new(NULL, NULL);
    gtk_container_add(GTK_CONTAINER(vbox_main), finestra_scrolled);
    GtkWidget *vbox=gtk_vbox_new(FALSE, 6);
    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (finestra_scrolled), vbox);
    /**Creo un pulsante per tornare indietro ed una label*/
    GtkWidget *pulsante_indietro=gtk_button_new_with_label("\nIndietro\n");
    gtk_box_pack_start (GTK_BOX (vbox_main), pulsante_indietro, FALSE, FALSE, 0);
    g_signal_connect(G_OBJECT(pulsante_indietro), "clicked", G_CALLBACK(distruggi), (gpointer)finestra);
    GtkWidget *label_piatto;


    /**Per ogni piatto ordinato dall'utente vado a creare una label contenente nome del piatto, quantit� e prezzo totale.
       La label viene poi inserita nella vbox creata in precedenza dentro la scrolled window.
       In questo modo creo una sorta di "scontrino digitale" (lo scontrino vero viene comunque salvato sul file)*/
    for(i=0; i<num_ordinazioni; i++)
    {
        sprintf(str_piatto, "\n%d x %s = %.2f", scontrini[numero_tavolo-1].ordini[i].quantita,
                                                scontrini[numero_tavolo-1].ordini[i].corrente.nome_piatto,
                                                (scontrini[numero_tavolo-1].ordini[i].quantita*scontrini[numero_tavolo-1].ordini[i].corrente.prezzo_piatto));
        label_piatto=gtk_label_new(str_piatto);
        gtk_box_pack_start (GTK_BOX (vbox), label_piatto, FALSE, FALSE, 0);
    }

    gtk_widget_show_all(finestra);                              ///Rendo visibile il tutto

    fclose(fp);                                                 ///Chiudo il file degli scontrini
    totale_scontrini(importo_totale);                           ///Invoco la funzione per aggiungere al totale degli scotrini l'importo totale appena calcolato
    aggiorna_scorte(num_ordinazioni, numero_tavolo, scontrini); ///Invoco la funzione per tener conto delle scorte utilizzate per le ordinazioni del tavolo corrente
    scontrini[numero_tavolo-1].id_tavolo=-1;                    ///Faccio sgomberare il tavolo, ora lo pu� occupare qualcun altro
    free(scontrini[numero_tavolo-1].ordini);                    ///Libero la memoria allocata per gli ordini del tavolo corrente
}

void totale_scontrini(float importo_totale)                                                     ///Questa procedura si occupa di aggiornare il totale degli importi di tutti gli scontrini
{
    FILE *fp=NULL;                                                      ///Creo un puntatore a file
    float tot_sc=0;                                                     ///Variabile che mi permette di tener conto del totale degli importi
    fp=fopen("Totale_scontrini.txt", "a+");                             ///Apro il file del totale degli scontrini in modalit� ibrida append
    rewind(fp);                                                         ///Mi sposto all'inizio del file
    if(fp==NULL)                                                        ///Controllo che il file sia stato aperto correttamente
    {
        printf("Errore nell'apertura del file totale_scontrini.txt\n");
        exit(-1);
    }
    fscanf(fp, "Totale scontrini: %f", &tot_sc);                        ///Leggo il totale dal file e lo memorizzo nella variabile tot_sc
    fclose(fp);                                                         ///Chiudo il file
    tot_sc+=importo_totale;                                             ///Aggiungo l'importo corrente al totale degli importi
    fp=fopen("Totale_scontrini.txt", "w");                              ///Riapro il file in modalit� lettura (sovrascrivo tutto)
    if(fp==NULL)                                                        ///Controllo che il file sia stato aperto correttamente
    {
        printf("Errore nell'apertura del file totale_scontrini.txt\n");
        exit(-1);
    }
    fprintf(fp, "Totale scontrini: %.2f", tot_sc);                      ///Stampo il totale aggiornato
    fclose(fp);                                                         ///Chiudo il file

}

/*** SPAZIO DEDICATO ALLE PROCEDURE/FUNZIONI AUSILIARIE (procedure che solitamente vengono richiamate all'interno di altre funzioni) ***/

void carica_piatti(Elementigtk *pacchetto)                                                      ///Questa procedura carica i piatti presenti nel file dei piatti in un vettore di piatti
{
    FILE *fp=NULL;                                              ///Dichiaro un puntatore a file
    int dim, i;                                                 ///..e due variabili intere, la prima indica il numero di piatti caricati, la seconda � un contatore
    printf("\nCarico i piatti da file...\n");
    fp=fopen ("piatti.dat", "rb");                              ///Apro il file binario piatti.dat in sola lettura
    if(fp==NULL)                                                ///Se il file non esiste, semplicemente esco dalla funzione
    {
        printf("Errore nell'apertura del file piatti.dat\n");
        return;
    }
    fseek(fp, 0, SEEK_END);                                     ///Mi sposto alla fine del file
    dim=ftell(fp)/sizeof(Piatto);                               ///Leggo la dimensione del file e la divido per la dimensione di Piatto al fine di trovare il numero di piatti presenti nel file
    printf("\nNel file ci sono %d piatti\n", dim);
    pacchetto->vettore=(Piatto*)calloc(dim, sizeof(Piatto));    ///Alloco dinamicamente dim*sizeof(Piatto) byte in memoria e li metto a disposizione del vettore..
                                                                ///..sul quale verranno caricati i piatti presenti nel file
    rewind(fp);                                                 ///Torno all'inizio del file
    for(i=0; i<dim; i++)                                        ///Per dim volte,
    {
        fread(&pacchetto->vettore[i], sizeof(Piatto), 1, fp);   ///Leggo il file e memorizzo ogni piatto presente nel vettore precedentemente allocato
    }
    pacchetto->dim_vettore=dim;                                 ///Salvo la dimensione del vettore in una variabile presente all'interno della struttura 'pacchetto'
    printf("\n%d piatti caricati con successo...\n", pacchetto->dim_vettore);
    fclose(fp);                                                 ///Chiudo il file
}

void prenota_pasto(GtkWidget *pulsante, Elementigtk *pacchetto)                                 ///Questa procedura si occupa di "prenotare" una tipologia di pasto in base al pulsante premuto dall'utente
{
    char stringa[20];                                               ///Dichiaro una stringa di 20 caratteri
    int n_tipologia;                                                ///e una variabile intera che mi serve per dividere i piatti in tipologie
    const gchar *testo=gtk_button_get_label((GtkButton*)pulsante);  ///Memorizzo il testo contenuto nel pulsante premuto dall'utente in una variabile di tipo gchar
    strcpy(stringa, testo);                                         ///Trasferisco il testo nella stringa precedentemente creata per evitare problemi di conversione
    sscanf(stringa, "%d", &n_tipologia);                            ///Ricerco un numero intero all'interno della stringa (indicante la tipologia del piatto) e lo memorizzo nella variabile n_tipologia
    switch(n_tipologia)                                             ///Uso l'istruzione switch per differenziare le tipologie
    {                                                               ///Per ogni tipologia memorizzo il valore corrispondente nella variabile di tipo enumerativo "tip_enum", che sar� visibile anche alle altre funzioni
    case 0:
        pacchetto->tip_enum=antipasto;
        break;
    case 1:
        pacchetto->tip_enum=primo;
        break;
    case 2:
        pacchetto->tip_enum=secondo;
        break;
    case 3:
        pacchetto->tip_enum=contorno;
        break;
    case 4:
        pacchetto->tip_enum=dessert;
        break;
    }
}

void carica_scorte(Elementigtk *pacchetto)                                                      ///Questa procedura mi permette, nel caso in cui il file delle scorte non esistesse o mancasse di qualche ingrediente, di creare/aggiornare il file a inizio programma
{
    FILE *fp=NULL;                                                              ///Dichiaro un puntatore a file
    int i, j, flag=0;                                                           ///Due puntatori a file
    Scorta buffer, temp;                                                        ///Dichiaro due variabili di tipo Scorta
    if((fp=fopen("scorte.dat", "r+b"))==0)                                      ///Apro il file delle scorte in modalit� ibrida lettura+scrittura, nel caso in cui il file non venga aperto correttamente..
    {
        printf("\nCREO IL FILE DELLE SCORTE\n");
        fp=fopen("scorte.dat", "wb");                                           ///Apro il file in modalit� lettura per creare il file
        fclose(fp);                                                             ///Chiudo il file
        fp=fopen("scorte.dat", "r+b");                                          ///e lo riapro in modalit� ibrida lettura + scrittura
    }
    printf("\nCARICO LE SCORTE\n");
    for(i=0; i<pacchetto->dim_vettore; i++)                                     ///Faccio scorrere tutte le ordinazioni di un tavolo
    {
        for(j=0; j<N_ING; j++)                                                  ///e per ogni ordinazione faccio scorrere tutti gli ingredienti del vettore
        {
            strcpy(buffer.nome, pacchetto->vettore[i].ingredienti_piatto[j]);   ///Copio l'ingrediente analizzato nel campo "nome" della variabile buffer
            buffer.quantita=0;                                                  ///e pongo la quantit� a zero, perch� il programma � stato appena eseguito, nessun tavolo ha ancora ordinato
            while(fread(&temp, sizeof(Scorta), 1, fp))                          ///Leggo il file dall'inizio e memorizzo ogni scorta presente nella variabile temporanea "temp"
            {
                //printf("buffer=%s, temp=%s\n", buffer.nome, temp.nome);
                if(strcmp(buffer.nome, temp.nome)==0)                           ///Per ogni scorta letta, se quest'ultima � corrispondente all'ingrediente letto dal vettore..
                {
                    flag=1;                                                     ///..inizializzo la flag a 1
                }
            }
            if(flag==0)                                                         ///Se l'ingrediente presente nel vettore di ordinazioni non � presente nel file delle scorte..
            {
                fseek(fp, 0, SEEK_END);                                         ///...mi sposto alla fine del file
                fwrite(&buffer, sizeof(Scorta), 1, fp);                         ///e scrivo l'ingrediente
            }
            flag=0;                                                             ///Riazzero la flag
            rewind(fp);                                                         ///e mi riposiziono all'inizio del file per iniziare di nuovo la scansione
        }
    }
    fclose(fp);                                                                 ///Chiudo il file
}

void inserisci_scorte(Piatto corrente)                                                          ///Questa procedura mi permette di aggiornare il file delle scorte ogni qual volta venga aggiunto un piatto
{
    FILE *fp=NULL;                                              ///Dichiaro un puntatore a file,
    int i, flag=0;                                              ///un contatore, una flag
    Scorta buffer, temp;                                        ///e due variabili di tipo scorta
    if((fp=fopen("scorte.dat", "r+b"))==0)                      ///Apro il file delle scorte in modalit� ibrida lettura+scrittura, nel caso in cui il file non venga aperto correttamente..
    {
        printf("\nCREO IL FILE DELLE SCORTE\n");
        fp=fopen("scorte.dat", "wb");                           ///Apro il file in modalit� lettura per creare il file
        fclose(fp);                                             ///Chiudo il file
        fp=fopen("scorte.dat", "r+b");                          ///e lo riapro in modalit� ibrida lettura + scrittura
    }
    printf("\nCARICO LE SCORTE\n");

        for(i=0; i<N_ING; i++)                                  ///Faccio scorrere il numero di ingredienti del piatto appena aggiunto
        {
            strcpy(buffer.nome, corrente.ingredienti_piatto[i]);///Copio ogni ingrediente del piatto nel campo "nome" della variabile buffer
            buffer.quantita=0;                                  ///e inizializzo la quantit� a zero
            while(fread(&temp, sizeof(Scorta), 1, fp))          ///
            {
                //printf("buffer=%s, temp=%s\n", buffer.nome, temp.nome);
                if(strcmp(buffer.nome, temp.nome)==0)           ///Leggo il file dall'inizio e memorizzo ogni scorta presente nella variabile temporanea "temp"
                {
                    flag=1;                                     ///..inizializzo la flag a 1
                }
            }
            if(flag==0)                                         ///Se l'ingrediente presente nel vettore di ordinazioni non � presente nel file delle scorte..
            {
                fseek(fp, 0, SEEK_END);                         ///...mi sposto alla fine del file
                fwrite(&buffer, sizeof(Scorta), 1, fp);         ///e scrivo l'ingrediente
            }
            flag=0;                                             ///Riazzero la flag
            rewind(fp);                                         ///e mi riposiziono all'inizio del file per iniziare di nuovo la scansione
        }
    fclose(fp);                                                 ///Chiudo il file
}

void aggiorna_scorte(int num_ordinazioni, int numero_tavolo, Scontrino scontrini[N_TAVOLI])     ///Questa procedura mi permette di aggiornare il file delle scorte ogni qual volta venga stampato uno scontrino
{
    Scorta buffer;                                              ///Dichiaro una variabile di tipo Scorta,
    int flag0=0, flag1=0, flag2=0, i=0;                         ///tre flag (una per ogni ingrediente) e un contatore
    FILE *fp=NULL;                                              ///Dichiaro un puntatore a file

    printf("\nNUM ORDINAZIONI=%d\n", num_ordinazioni);
    if((fp=fopen("scorte.dat", "r+b"))==NULL)                   ///Come nelle funzioni precedenti, se il file non esiste lo creo e lo riapro
    {
        printf("\nCREO IL FILE scorte.dat\n");
        fp=fopen("scorte.dat", "wb");
        fclose(fp);
        fp=fopen("scorte.dat", "r+b");
    }

        for(i=0; i<num_ordinazioni; i++)                        ///Faccio scorrere tutte le ordinazioni prese per un certo tavolo
        {
            while(fread(&buffer, sizeof(Scorta), 1, fp)==1)     ///Leggo il file delle scorte e memorizzo la scorta letta nella variabile buffer
            {
                printf("\ni=%d, buffer=%s\n", i, buffer.nome);
                /**Controllo se qualcuno tra gli ingredienti presenti nell'ordinazione "analizzata" coincida con un ingrediente presente nel file delle scorte, se � presente..*/
                if(strcmp(buffer.nome,scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[0])==0 && flag0==0)
                {
                    fseek(fp, -sizeof(Scorta), SEEK_CUR);                               ///Torno indietro di una posizione (perch� devo sovrascrivere la quantit�)
                    buffer.quantita+=scontrini[numero_tavolo-1].ordini[i].quantita;     ///Incremento la quantit� dell'ingrediente utilizzato
                    printf("SCRITTURA=%d\n", fwrite(&buffer, sizeof(Scorta), 1, fp));
                    flag0=1;                                                            ///Inizializzo a 1 la flag relativa all'ingrediente
                    printf("\nTROVATO UNO\n");
                    fseek(fp, -sizeof(Scorta), SEEK_CUR);                               ///Torno indietro di una posizione per evitare di saltare posizioni
                }
                if(strcmp(buffer.nome,scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[1])==0 && flag1==0)  ///..idem per questo if
                {
                    fseek(fp, -sizeof(Scorta), SEEK_CUR);
                    buffer.quantita+=scontrini[numero_tavolo-1].ordini[i].quantita;
                    printf("SCRITTURA=%d\n", fwrite(&buffer, sizeof(Scorta), 1, fp));
                    flag1=1;
                    printf("\nTROVATO DUE\n");
                    fseek(fp, -sizeof(Scorta), SEEK_CUR);
                }
                if(strcmp(buffer.nome,scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[2])==0 && flag2==0)  ///..e per questo
                {
                    fseek(fp, -sizeof(Scorta), SEEK_CUR);
                    buffer.quantita+=scontrini[numero_tavolo-1].ordini[i].quantita;
                    printf("SCRITTURA=%d\n", fwrite(&buffer, sizeof(Scorta), 1, fp));
                    flag2=1;
                    printf("\nTROVATO TRE\n");
                    fseek(fp, -sizeof(Scorta), SEEK_CUR);
                }
                if(flag0==1 && flag1==1 && flag2==1)                                    ///Se tutti i 3 ingredienti sono stati aggiunti..
                {
                    rewind(fp);                                                         ///torno all'inizio del file
                    break;                                                              ///ed esco dal ciclo for
                }
            }
            /**Queste tre istruzioni if sono simili a quelle di prima, ma servono ad aggiungere le scorte nel caso in cui gli ingredienti analizzati non siano presenti nel
               file delle scorte (per esempio dopo aver eseguito la funzione elimina_scorte)*/
            if (flag0==0)                                                                                           ///Se l'ingrediente non � presente nel file delle scorte..
            {
                printf("\nStai usando per la prima volta un ingrediente!\n");
                fseek(fp, 0, SEEK_END);                                                                             ///mi sposto alla fine del file
                printf("ingrediente=%s\n", scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[0]);
                strcpy(buffer.nome, scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[0]);           ///copio l'ingrediente nel campo nome della variabile buffer
                buffer.quantita=scontrini[numero_tavolo-1].ordini[i].quantita;                                      ///e la quantit� nel campo quantita
                fwrite(&buffer, sizeof(Scorta), 1, fp);                                                             ///Scrivo l'ingrediente + quantit� sul file delle scorte
                flag0=1;                                                                                            ///Inizializzo la flag a 1
                fseek(fp, i*sizeof(Scorta), SEEK_SET);                                                              ///Risposto il cursore nella posizione in cui era prima di effettuare la scrittura
            }
            if (flag1==0)                                                                                           ///idem per questo if..
            {
                printf("\nStai usando per la prima volta un ingrediente!\n");
                fseek(fp, 0, SEEK_END);
                printf("ingrediente=%s\n", scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[1]);
                strcpy(buffer.nome, scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[1]);
                buffer.quantita=scontrini[numero_tavolo-1].ordini[i].quantita;
                fwrite(&buffer, sizeof(Scorta), 1, fp);
                flag1=1;
                fseek(fp, i*sizeof(Scorta), SEEK_SET);
            }
            if (flag2==0)                                                                                           ///..e per questo
            {
                printf("\nStai usando per la prima volta un ingrediente!\n");
                fseek(fp, 0, SEEK_END);
                printf("ingrediente=%s\n", scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[2]);
                strcpy(buffer.nome, scontrini[numero_tavolo-1].ordini[i].corrente.ingredienti_piatto[2]);
                buffer.quantita=scontrini[numero_tavolo-1].ordini[i].quantita;
                fwrite(&buffer, sizeof(Scorta), 1, fp);
                flag2=1;
                fseek(fp, i*sizeof(Scorta), SEEK_SET);
            }
            flag0=0;                                                                                                ///Azzero le tre flag
            flag1=0;
            flag2=0;
        }

    fclose(fp);                                                                                                     ///Chiudo il file
}

void fai_il_conto(Scontrino scontrini[N_TAVOLI], int numero_tavolo, FILE *fp, Tipologie tipo)   ///Questa procedura si occupa di calcolare e stampare su file l'importo di ogni singolo piatto ordinato da un tavolo specifico, tenendo conto delle quantit�
{
    int i, num_ordinazioni=scontrini[numero_tavolo-1].num_ordinazioni;              ///Dichiaro un contatore  e una variabile nella quale memorizzo il numero di ordinazioni del tavolo corrente
    for(i=0; i<num_ordinazioni; i++)                                                ///Faccio scorrere tutte le ordinazioni del tavolo
        if(scontrini[numero_tavolo-1].ordini[i].corrente.tipologia_piatto==tipo)    ///e se la tipologia corrisponde a quella selezionata
        {
            fprintf(fp, " - %d x", scontrini[numero_tavolo-1].ordini[i].quantita);  ///stampo su file la quantit�, il nome e il prezzo
            fprintf(fp, "\t%-100s", scontrini[numero_tavolo-1].ordini[i].corrente.nome_piatto);
            fprintf(fp, "\t%5.2f�\n" ,scontrini[numero_tavolo-1].ordini[i].quantita*scontrini[numero_tavolo-1].ordini[i].corrente.prezzo_piatto);
            /**Infine aggiungo al totale dello scontrino corrente l'importo del piatto corrente*/
            scontrini[numero_tavolo-1].prezzo_totale+=scontrini[numero_tavolo-1].ordini[i].quantita*scontrini[numero_tavolo-1].ordini[i].corrente.prezzo_piatto;
        }
}

int controlla_se_modificabile(Elementigtk *pacchetto, Piatto risultato, GtkWidget *win)         ///Questa funzione controlla che al momento della modifica/eliminazione di un piatto questo non sia gi� presente nella lista di ordinazioni di tutti i tavoli per prevenire la modifica
{
    int i, j;                                                                                   ///Dichiaro due contatori
    for(i=0; i<N_TAVOLI; i++)                                                                   ///Faccio scorrere tutti i tavoli
    {
        for(j=0; j<pacchetto->punt_scontrini[i].num_ordinazioni; j++)                           ///e per ogni tavolo tutte le ordinazioni
        {
            if(risultato.id_piatto==pacchetto->punt_scontrini[i].ordini[j].corrente.id_piatto   ///Se il piatto da modificare � presente nel vettore delle ordinazioni, avviso l'utente ed esco restituendo 0 come valore
               && pacchetto->punt_scontrini[i].id_tavolo!=-1)
            {
                printf("Il piatto non puo' essere modificato al momento\n");
                pacchetto->dialog=gtk_message_dialog_new (GTK_WINDOW (win),GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "Il piatto non puo' essere modificato/eliminato\nal momento perche' un tavolo lo ha richiesto!");
                gtk_dialog_run(GTK_DIALOG(pacchetto->dialog));
                gtk_widget_destroy(pacchetto->dialog);
                return 0;
            }
        }
    }
    return 1;                                                                                   ///Altrimenti sco e restituisco 1
}

Piatto controlla_nome(GtkWidget *entry, Elementigtk *pacchetto)                                 ///Questa funzione verifica che il piatto richiesto dall'utente in caso di modifica/eliminazione sia presente nel menu. Nel caso sia presente, restituisce una variabile di tipo Piatto contenente il piatto da modificare/eliminare
{
    int i, j, dim=pacchetto->dim_vettore, flag=0;                                               ///Dichiaro due contatori, una variabile in cui salvo la dimensione del vettore di piatti e una variabile che user� come flag
    Piatto buffer;                                                                              ///Dichiaro una variabile di tipo Piatto che mi servir� come variabile di ritorno della funzione
    buffer.id_piatto=0;                                                                         ///Inizializzo l'id della variabile a zero perch� voglio evitare che assuma dei valori casuali (in particolare -1)
    char stringa[NOME];                                                                         ///Dichiaro una stringa
    strcpy(stringa, gtk_entry_get_text((GtkEntry*)entry));                                      ///nella quale memorizzo il testo contenuto nella entry
    printf("\n---------CONTROLLA NOME----------\n");
    for(i=0; i<dim; i++)                                                                        ///Faccio scorrere tutto il vettore
    {
        for(j=0; j<strlen(pacchetto->vettore[i].nome_piatto); j++)                              ///e tutte le lettere contenute nel nome del piatto
        {
            printf("%c - %c\n", tolower(pacchetto->vettore[i].nome_piatto[j]), tolower(stringa[j]));
            if(tolower(pacchetto->vettore[i].nome_piatto[j]) != tolower(stringa[j]) ||          ///Se la lettera corrente del piatto analizzato non corrisponde alla lettera corrente del piatto inserito
               strlen(pacchetto->vettore[i].nome_piatto) != strlen(stringa))                    ///o se il numero di lettere dei due piatti semplicemente non coincide
            {
                flag=0;                                                                         ///imposto la flag a 0 ed esco dal ciclo
                break;
            }
            else                                                                                ///Altrimenti
            {
                flag++;                                                                         ///incremento la flag
                printf("flag=%d, strlen(%s)=%d\n", flag, pacchetto->vettore[i].nome_piatto, strlen(pacchetto->vettore[i].nome_piatto));
                if(flag==strlen(pacchetto->vettore[i].nome_piatto))                             ///Se i due nomi coincidono..
                {
                    buffer=pacchetto->vettore[i];                                               ///Copio il piatto nella variabile buffer
                    printf("\nControlla nome: Ok, il nome esiste! buffer.id_piatto=%d\n", buffer.id_piatto);
                    return (buffer);                                                            ///e restituisco la variabile buffer in uscita
                }
            }
        }
    }

    buffer.id_piatto=-1;                                                                        ///Altrimenti imposto l'id a -1

    return buffer;                                                                              ///E restituisco sempre la variabile buffer
}

void dealloca(Scontrino *scontrini)                                                             ///Questa procedura viene invocata al termine dell'esecuzione del programma e si occupa di deallocare tutto ci� che non � stato ancora deallocato e di terminare il processo relativo alle GTK
{
    int i;                          ///Dichiaro un contatore
    for(i=0; i<N_TAVOLI; i++)       ///Faccio scorrere tutti i tavoli
    {
        free(scontrini[i].ordini);  ///Libero la memoria allocata per tutti gli ordini di ogni singolo tavolo
    }
    free(scontrini);                ///Libero la memoria allocata per il vettore di scontrini (Tutto il resto � stato deallocato in altri punti del programma)
    gtk_main_quit();                ///Chiudo il processo GTK
}

